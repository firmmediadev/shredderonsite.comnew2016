<div id="mini-features">
<div class="mini-inside">
	<div class="col-full">

		    
	<div class="block s-c">
	        	<a href="/services/shredding-services/#scs"><img class="home-icon" alt="" src="<?=FILES?>/images/calendar.png"></a>				
            <div class="feature">
       <h3><a href="/services/shredding-services/#scs">SCHEDULED CONTAINER</a></h3>
       <p>daily, weekly, monthly, or on-call destruction services</p>
               </div><!--/.feature-->
	</div><!--/.block-->      
		
	<div class="block p-b-c">
	        	<a href="/services/shredding-services/#purge"><img class="home-icon" alt="" src="<?=FILES?>/images/recycle-bin.png"></a>				
            <div class="feature">
       <h3><a href="/services/shredding-services/#purge">PURGE/FILE BOX CLEANOUT</a></h3>
       <p>annual, semi-annual or periodic purges of old records</p>
               </div><!--/.feature-->
	</div><!--/.block-->      
		
	<div class="block d-o">
	        	<a href="/services/shredding-services/#drop"><img class="home-icon" alt="" src="<?=FILES?>/images/box.png"></a>			
            <div class="feature">
       <h3><a href="/services/shredding-services/#drop">DROP OFF</a></h3>
       <p>bring records to our facility&nbsp;for&nbsp; shredding</p>
               </div><!--/.feature-->
	</div><!--/.block-->   
	
	<div class="block w-d">
		        	<a href="/services/shredding-services/#witness"><img class="home-icon" alt="" src="<?=FILES?>/images/search.png"></a>				
                <div class="feature">
           <h3><a href="/services/shredding-services/#witness">WITNESSED DESTRUCTION</a></h3>
           <p>witness the immediate destruction of the material</p>
                   </div><!--/.feature-->
	</div><!--/.block-->     
		<div class="fix"></div><!--/.fix-->
	</div><!--/.col-full-->
	</div>
</div>