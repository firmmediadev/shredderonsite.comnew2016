jQuery(document).ready(function(){
	jQuery('.newSlides_container').slick({
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		autoplay: true,
		autoplaySpeed: 6000
	});
});