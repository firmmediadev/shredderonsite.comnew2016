<div id="newSlider">



<div id="slide-box">
	<div class="newSlides_container">
				
		<div class="slide">
			<div class="entry slide-content fl">
				<h2 class="title"><a href="/services/" title="Excellent Services at Great Prices">Excellent Services at Great Prices</a></h2>
				<p>We work with you to identify which of our services will fit both your needs and your budget. In addition, we provide special offers to customers who sign long-term contracts. </p>
				<p><a class="slider-green" href="/services/">Learn about our in-depth services »</a></p>
			</div><!--/.entry-->
		
			<div class="slide-image fl">
				<a href="/services/" title="Excellent Services at Great Prices"><img src="http://shredderonsite.com/wp-content/uploads/2011/10/services.png" alt="" width="900" height="338" class="woo-image slide-img"></a>
			</div><!--/.slide-image-->
			
			<div class="fix"></div><!--/.fix-->
	
		</div>

		<div class="slide">
			<div class="entry slide-content fl">
				<h2 class="title"><a href="/services/#type" title="What to Shred">What to Shred</a></h2>
				<p>CDDC protects your company and customers by keeping you in compliance with federal privacy laws. We destroy paper documents and bindings, electronic storage devices, and other mixed media.</p>
				<p><a class="slider-green" href="/services/#type">Review the list of items we shred and recycle »</a></p>
			</div><!--/.entry-->
			
			<div class="slide-image fl">
				<a href="/services/#type" title="What to Shred"><img src="http://shredderonsite.com/wp-content/uploads/2011/11/filestack.png" alt="" width="900" height="338" class="woo-image slide-img"></a>	
			</div><!--/.slide-image-->
			
			<div class="fix"></div><!--/.fix-->
		</div>
		
		<div class="slide">
			<div class="entry slide-content fl">
			<h2 class="title"><a href="/services/e-wasteelectronic-media-destruction-services/" title="Electronic Waste">Electronic Waste</a></h2>
			<p>You don’t simply toss old electronics in the trash, you have to dispose of them properly as eWaste. Electronic waste can include hard drives, zip drives, CD’s and DVD’s even old reel-to-reel and VHS tapes.</p>
			<p>Scheduled Onsite E-waste Destruction $1 a pound.(Minimums apply based on service)</p>
			<p>&nbsp;</p>
			<p><a href="tel:8888262332">Call (888)826-2332 for details and exclusions »</a></p>
		</div><!--/.entry-->
		
		<div class="slide-image fl">
			<a href="/services/e-wasteelectronic-media-destruction-services/" title="Electronic Waste"><img src="http://shredderonsite.com/wp-content/uploads/2015/02/electronicWaste3.png" alt="" width="900" height="338" class="woo-image slide-img"></a>	
		</div><!--/.slide-image-->
		
		<div class="fix"></div><!--/.fix-->
		</div>
		
		<div class="slide">
			<div class="entry slide-content fl">
				<h2 class="title"><a href="/testimonials/" title="What Our Customers Say…">What Our Customers Say…</a></h2>
				<p>For years, we have provided reliable, professional services to a host of clients, from national Fortune 500 companies to small businesses in Southern California. </p>
				<p><a class="slider-green" href="/testimonials/">Here’s what they have to say about us »</a></p>
			</div><!--/.entry-->
	
			<div class="slide-image fl">
				<a href="/testimonials/" title="What Our Customers Say…"><img src="http://shredderonsite.com/wp-content/uploads/2011/11/customers.png" alt="" width="900" height="338" class="woo-image slide-img"></a>	
			</div><!--/.slide-image-->
			
			<div class="fix"></div><!--/.fix-->
		</div>
		
		<div class="slide">
			<div class="entry slide-content fl">
				<h2 class="title"><a href="/contact/" title="Your Time Matters to Us">Your Time Matters to Us</a></h2>
				<p>We respond quickly to your urgent data destruction needs. Our experienced staff and fleet of trucks will handle your request on-time and in time no matter the size or complexity.</p>
				<p><a class="slider-green" href="/contact/">Get in touch to learn more »</a></p>
			</div><!--/.entry-->
			
			<div class="slide-image fl">
				<a href="/contact/" title="Your Time Matters to Us"><img src="http://shredderonsite.com/wp-content/uploads/2011/11/watch.png" alt="" width="900" height="338" class="woo-image slide-img"></a>	
			</div><!--/.slide-image-->
			
			<div class="fix"></div><!--/.fix-->
			
		</div>
		
		<div class="slide">
			<div class="entry slide-content fl">
				<h2 class="title"><a href="/contact/" title="We Serve Southern California">We Serve Southern California</a></h2>
				<p>As a local provider of data destruction, we have spent years investing in relationships with Southern California businesses. Serving: Ventura, San Fernando Valley, Santa Clarita, Los Angeles, Pasadena, Santa Monica and surrounding areas</p>
				<p><a class="slider-green" href="/contact/">Contact us about helping with your company’s needs »</a></p>
			</div><!--/.entry-->
			
			<div class="slide-image fl">
				<a href="/contact/" title="We Serve Southern California"><img src="http://shredderonsite.com/wp-content/uploads/2011/10/map2.png" alt="" width="900" height="338" class="woo-image slide-img"></a>	
			</div><!--/.slide-image-->
			
			<div class="fix"></div><!--/.fix-->
		</div>
		

			
			</div> <!--/.slides_container .col-full-->
	</div> <!-- end .slide-box -->
</div>	<!-- end #newSlider -->
<?php //echo 'hello world';?>