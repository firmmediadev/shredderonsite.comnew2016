<?php
/*
Plugin Name: Oktopost Future Posts
Version: 1.0
Description: Allow Oktopost to access future posts 
Author: Liad Guez
Author URI: http://www.oktopost.com
*/

class Oktopost_Future_Posts {

	const OKTO_AGENT = 'Embedly';


	public static function allow_future_posts() {
		if (self::is_allowed()) {
			self::register_status();
		}
	}


	private static function is_allowed() {
		$userAgent = self::get_user_agent();		
		return strpos($userAgent, self::OKTO_AGENT) !== false && is_single();
	}

	private static function get_user_agent() {
		return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	}

	private static function register_status() {
		register_post_status('future', array(
			'label'       => _x('Scheduled', 'post'),
			'protected'   => false,
			'public' 	  => true,
			'_builtin'    => true,
			'label_count' => _n_noop(
				'Scheduled <span class="count">(%s)</span>', 
				'Scheduled <span class="count">(%s)</span>' 
			),
		));
	}
}

add_action('pre_get_posts', 'Oktopost_Future_Posts::allow_future_posts', 1);