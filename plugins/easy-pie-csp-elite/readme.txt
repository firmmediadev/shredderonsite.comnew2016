=== Coming Soon Page Elite ===
Contributors: bobriley
Donate link: http://easypiewp.com/donate/
Tags: coming soon, coming soon page, construction, landing page, launch, launch page, maintenance, maintenance mode, offline, unavailable, under construction, underconstruction, wordpress coming soon, wordpress maintenance mode, wordpress under construction
Requires at least: 3.5
Tested up to: 4.1
Stable tag: 1.1.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Coming Soon Page Elite lets visitors know that your website is 'Coming Soon' while it collects emails for you!

== Description ==

Let your visitors know your site is Coming Soon while gathering their contact information.

### Basic Features
* Professional look
* Highly customizable
* Fully Responsive
* Same simplicity and power of the free version with powerful extras.
* Access Control: Give some people site access while collecting emails from others.
* Optional countdown timer
* No HTML required
* CSS customizable but not required
* Use built in backgrounds or your own to give Coming Soon Page a great look
* Add your own logo
* Preview from Admin panel
* Subscriber list shows name and emails of visitors
* Simple indicator easily lets you know when Coming Soon mode is on
* Easily translatable
* Facebook Support 
* Google+ Support
* Twitter Support
* Instagram Support
* LinkedIn Support
* Pinterest Support
* SoundCloud Support
* YouTube Support
* Choose either a 503 or 200 HTTP status when in Coming Soon mode
* Selectively disable Coming Soon Page on certain URLs

### Advanced Features
* Export email addresses gathered by Coming Soon Page to CSV 
* Automatically sync subscribed emails to MailChimp

### Overview
Coming Soon Page is a great way to let visitors know your site is Coming Soon.  When Coming Soon Page is displayed it collects valuable emails from potential customers in a friendly and professional way.  Coming Soon Page is also theme independent, having been confirmed to work well with top WordPress themes. 

Finally, your visitors won't be just shown your web host's parked domain page, instead they'll know your site is truly 'Coming Soon'!

== Installation ==

= Using The WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Search for 'Coming Soon Page'
3. Click 'Install Now'
4. Activate the plugin on the Plugin dashboard

= Uploading in WordPress Dashboard =

1. Navigate to the 'Add New' in the plugins dashboard
2. Navigate to the 'Upload' area
3. Select `easy-pie-csp-elite.zip` from your computer
4. Click 'Install Now'
5. Activate the plugin in the Plugin dashboard

= Using FTP =

1. Download `easy-pie-coming-soon.zip`
2. Extract the `easy-pie-csp-elite` directory to your computer
3. Upload the `easy-pie-csp-elite` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin's dashboard

== Frequently Asked Questions ==

For the FAQ on the Coming Soon Page please visit the [Snap Creek Website](http://snapcreek.com/ezp-coming-soon/).

== Screenshots ==

1. Example Coming Soon page 
2. Display configuration
3. Content configuration
4. Settings
5. Subscriber list

== Changelog ==
= 2.1.0 =
* New: Add Constant Contact
= 1.1.0 =
* New: Countdown timer text now settable from Template/Content tab
* New: Countdown timer date moved to Settings/General tab
* New: Invalid licenses now create admin alert
* New: Social icons now have ids for easier CSS styling (see https://easypiewp.com/coming-soon-page-elite-css-styling/)
* Fix: License screen easier to use, more reliable activation logic
* Fix: Subscriber table now UTF-8 for new installs. Can help with names with international characters.

= 1.0.2 =
* Added admin email alerts for new subscribers

= 1.0.1 =
* Fixed URL filter save bug (Thanks Carrie F)
* Added time for Countdown timer (Thanks Goran and Siddharth)

= 1.0.0 =
* Initial release

== Upgrade Notice ==

= 1.1.0 =
* Countdown timer text is now settable, improvements in license activation logic and easier to style social icons. Important: Countdown timer has now moved to the Settings/General tab.

= 1.0.2 =
* Added admin email alerts for new subscribers

= 1.0.1 =
* Fixed URL filter save bug (Thanks Carrie F)
* Added time for Countdown timer (Thanks Goran and Siddharth)

= 1.0.0 =
* Initial release
