<?php
/* @var $display EZP_CSPE_Display_Entity */
$global = EZP_CSPE_Global_Entity::get_instance();

$set_index = $global->active_set_index;

$set = EZP_CSPE_Set_Entity::get_by_id($set_index);

$display = EZP_CSPE_Display_Entity::get_by_id($set->display_index);

$content = EZP_CSPE_Content_Entity::get_by_id($set->content_index);

$config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);

$error_display = 'none';
$error_text = '';

$js_thank_you = "var thankYouDisplayed=false;";

$name_required = $config->collect_name ? 'require' : '';

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    check_admin_referer( 'cspe-main' );

	if($config->collect_email === false)
	{
		die('Invalid');
	}

    $subscriber = new EZP_CSPE_Subscriber_Entity();
    $subscriber->friendly_name = substr($_POST['EZP_CSPE_name'], 0, 255);
    $subscriber->email_address = $_POST['EZP_CSPE_email'];

    $email_verifier = new EZP_CSPE_Email_Verifier(false, EZP_CSPE_U::__('Invalid Email'));

    $error_text = $email_verifier->Verify($subscriber->email_address);


    if(!ctype_xdigit($subscriber->friendly_name) && (!is_numeric($subscriber->friendly_name)))
    {
        // Let non numeric stuff through otherwise just quietly let it pass
        if ($error_text == '')
        {
            if($config->collect_name)
            {
                $name_verifier = new EZP_CSPE_Required_Verifier(EZP_CSPE_U::__('Invalid Name'));

                $error_text = $name_verifier->Verify($subscriber->friendly_name);
            }

            if ($error_text == '')
            {
                if ($subscriber->save())
                {
                    if ($config->send_email_on_subscribe)
                    {
                        EZP_CSPE_Email_Utility::send_admin_subscribe_email($subscriber);
                    }

                    EZP_CSPE_Email_Utility::add_subscriber_to_mail_providers($subscriber);
                }
                else
                {
                    // On at least one database there was a problem with the name. Want to ensure we have this quick hack in there for him and maybe others
                    $subscriber->friendly_name = '';

                    if ($subscriber->save())
                    {
                        if ($config->send_email_on_subscribe)
                        {
                            EZP_CSPE_Email_Utility::send_admin_subscribe_email($subscriber);
                        }

                        EZP_CSPE_Email_Utility::add_subscriber_to_mail_providers($subscriber);
                    }
                    else
                    {
                        $error_text = sprintf(EZP_CSPE_U::__('Problem saving subscriber %1$s with email %2$s to database.'), $subscriber->friendly_name, $subscriber->email_address);
                    }
                }
            }
        }
    }

    if ($error_text == '')
    {

        //EZP_CSPE_Email_Utility::add_new_subscriber()
        $js_thank_you = "var thankYouDisplayed=true;";
        $initial_section_display = 'none';
        $thank_you_section_display = 'block';
    }
    else
    {

        $error_display = 'block';
        $initial_section_display = 'block';
        $thank_you_section_display = 'none';
    }
}
else
{

    $initial_section_display = 'block';
    $thank_you_section_display = 'none';
}

$google_font_stylesheet_text = $display->get_google_font_stylesheet_text();

?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Title here -->
        <title><?php echo $content->title; ?></title>

        <?php
        echo "

        <meta name='description' content='$config->meta_description'>

        <meta name='keywords' content='$config->meta_keywords'>

        <link rel='author' href='$config->author_url' />"
        ?>        

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--Fonts-->
        <!-- SEtting: {font-link-list} - list of all fonts referenced in the template -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>-->

        <!-- Styles -->

		<!-- Google Fonts CSS -->
		<?php echo $google_font_stylesheet_text; ?>
		
        <!-- Bootstrap CSS -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font awesome CSS -->
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">       

        <!-- Custom CSS -->
        <link href="<?php echo $page_url . '/css/style.css?' . EZP_CSPE_Constants::PLUGIN_VERSION ?>" rel="stylesheet">       

        <style type="text/css">
<?php
list($content_box_red, $content_box_green, $content_box_blue) = sscanf($display->content_box_color, "#%02x%02x%02x");

switch($display->background_type)
{
	case EZP_CSPE_Display_Background_Type::Color:
		$background_color = "background-color: $display->background_color;";
		break;
	
	case EZP_CSPE_Display_Background_Type::Image:
		$background_color = "";
		break;
	
	case EZP_CSPE_Display_Background_Type::Video:
		$background_color = "";
		break;
}

if($display->background_type == EZP_CSPE_Display_Background_Type::Image)
{
	if ($display->background_tiling_enabled == 'true')
	{

		$background_size_styles = "body { background-image: url('$display->background_image_url'); $background_color }";
	}
	else
	{
		$background_size_styles = "{ margin: 0; padding: 0; }
												body {                                                 
													 background: url('$display->background_image_url') no-repeat center center fixed;
													$background_color                                               
													-webkit-background-size: cover;
													-moz-background-size: cover;
													-o-background-size: cover;
													background-size: cover;
												}";
	}
}
else
{
	$background_size_styles = "{ margin: 0; padding: 0; }
												body {                                                 
													$background_color                                               
													-webkit-background-size: cover;
													-moz-background-size: cover;
													-o-background-size: cover;
													background-size: cover;
												}";
}

$email_display = EZP_CSPE_Render_Utility::get_display($config->collect_email, "block");
$name_display = EZP_CSPE_Render_Utility::get_display($config->collect_name, "inline-block");

$logo_display = EZP_CSPE_Render_Utility::get_display($content->logo_url, "inline");

if (trim($display->logo_width) == "")
{

    $logo_width_adjustment = "";
}
else
{

    $logo_width_adjustment = "width: $display->logo_width;";
}

if (trim($display->logo_height) == "")
{

    $logo_height_adjustment = "";
}
else
{

    $logo_height_adjustment = "height: $display->logo_height;";
}

if (trim($config->countdown_due_date) == "")
{

    $countdown_display = "none";
}
else
{

    $countdown_display = "block";
}

if($display->box_shadow_enable)
{
	$box_shadow = "box-shadow: 1px 7px 36px -5px rgba(34,34,34,1);";
}
else
{
	$box_shadow = '';
}

echo "
            
            $background_size_styles            
            #content-area { $box_shadow; background:rgba($content_box_red, $content_box_green, $content_box_blue, $display->content_box_opacity); }           
            #headline { {$display->get_font_styling('text_headline')}  }     
			#thank-you-headline { {$display->get_font_styling('text_thankyou_headline')}  }     
            #description, #thank-you-text { {$display->get_font_styling('text_description')}  }
            #disclaimer { {$display->get_font_styling('text_disclaimer')}  }
            #footer { {$display->get_font_styling('text_footer')}  }
                        
            #logo { display:$logo_display; $logo_height_adjustment; $logo_width_adjustment;  }
            #email-submit-button { margin-left:3px; {$display->get_font_styling('email_button')}; background-color: $display->email_button_color; height: $display->email_button_height; width: $display->email_button_width; }
            
            #email-collection-box { display:$email_display; }
            #name-form-group { display:$name_display; }
            #email-form-group { margin-left:auto;margin-right:auto;}
            /* #email-form-group, #name-form-group { width: 180px; }*/
            /* #name-input, #email-input { width: 180px; } */
                
            #initial-section { display:$initial_section_display; }
            #thank-you-section { display: $thank_you_section_display; }
            #error-block { display: $error_display; color:red; margin-top:5px; }
            #countdown { display: $countdown_display; }
              
            /* Custom CSS */
            $display->css
            ";
?>
        </style>

        <script type="text/javascript">
<?php echo $js_thank_you; ?>
             <?php 
                $local_timestamp = strtotime($config->countdown_due_date) . ' ';
                
                // local time stamp is expressed as it were GMT so we have to reverse it.                
                $reverse_gmt_timestamp = $local_timestamp - (3600 * get_option('gmt_offset'));
            ?>
                
            // Clock end date needs to be in gmt
            clockEndDate =  "<?php echo date('Y/m/d H:i:s', $reverse_gmt_timestamp) . ' UTC'; ?>";
                    
        </script>

        <!-- Header Scripts -->
        <?php echo $config->analytics_code; ?>
    </head>

    <body>
        <div id="youtube-player">
 
        <div class="container">	

            <!-- Subscribe Starts -->
            <div id="content-area" class="text-center">

                <img id="logo" src="<?php echo $content->logo_url ?>"/> 


                <div id="initial-section">
                    <header class="text-center">
                        <!-- Setting: {{headline}} -->
						
                        <h1 id="headline" class="<?php echo $display->get_font_effect_class('text_headline'); ?>"><?php echo $content->headline ?></h1>

                        <!-- Setting: {{description}} -->                   
                        <p id="description" class="<?php echo $display->get_font_effect_class('text_description'); ?>"><?php echo $content->description; ?></p>

                        <p id="custom-html" style="display:"><!--Setting: {{custom-html}} --></p>
                    </header>

                    <div id="countdown"></div>

                    <form id="email-collection-box" name="email-collection-box" class="form-inline" role="form" action="" method="post">

                        <?php
							if($config->collect_email)
							{
								wp_nonce_field('cspe-main');
							}
						?>

                        <!-- Setting: {{name-collection-on}}-->
                        <div id="name-form-group" class="form-group">
                            <label class="sr-only" for="EZP_CSPE_name"><?php EZP_CSPE_U::_e("Name"); ?></label>
                            <!-- Setting: {{name-placeholder}}-->
                            <input id="name-input" <?php echo $name_required; ?> name="EZP_CSPE_name" type="text" class="form-control" placeholder="<?php echo $content->name_placeholder_text; ?>"/>
                        </div>
                        <div id="email-form-group" class="form-group">
                            <label class="sr-only" for="EZP_CSPE_email"><?php EZP_CSPE_U::_e("Email"); ?></label>                            
                            <input id="email-input" required name="EZP_CSPE_email" type="email" class="form-control" placeholder="<?php echo $content->email_placeholder_text; ?>"/>
                        </div>

                        <button id="email-submit-button" form="email-collection-box" type="submit" class="btn btn-danger <?php echo $display->get_font_effect_class('email_button'); ?>"><?php echo $content->email_button_text; ?></button>                        
                        <div id="error-block"><?php
                            if ($error_text != '')
                            {
                                echo $error_text;
                            }
                            ?></div>
                        <p id="disclaimer" class="<?php echo $display->get_font_effect_class('text_disclaimer'); ?>"><?php echo $content->disclaimer; ?></p>
                    </form>
                </div>
                <div id="thank-you-section">
                    <header class="text-center">
                        <!-- Setting: {{thank-you-headline}} -->
																		
                        <h1 id="thank-you-headline" class="<?php echo $display->get_font_effect_class('text_thankyou_headline'); ?>" ><?php echo $content->thank_you_headline; ?></h1>

                        <!-- Setting: {{thank-you-text}} -->                   
                        <p id="thank-you-text"><?php echo $content->thank_you_description; ?></p>

                        <p id="custom-html"><!--Setting: {{custom-html}} --></p>
                    </header>                    
                </div>

            </div>

            <!-- Social Networks -->
            <div id="social" class="text-center">
                <a id="social-facebook" target="_blank" href="<?php echo $config->facebook_url ?>" style="background-color:#3C599F; display:<?php echo EZP_CSPE_Render_Utility::get_display($config->facebook_url, "inline-block"); ?>"><i class="fa fa-facebook"></i></a>                                
                <a id="social-google-plus" target="_blank" href="<?php echo $config->google_plus_url ?>" style="background-color:#CF3D2E; display:<?php echo EZP_CSPE_Render_Utility::get_display($config->google_plus_url, "inline-block"); ?>"><i class="fa fa-google-plus"></i></a>                
                <a id="social-instagram" target="_blank" href="<?php echo $config->instagram_url ?>" style="background-color: #A1755C; display:<?php echo EZP_CSPE_Render_Utility::get_display($config->instagram_url, "inline-block"); ?>"><i class="fa fa-instagram"></i></a>
                <a id="social-linkedin" target="_blank" href="<?php echo $config->linkedin_url ?>" style="background-color: #0085AE; display:<?php echo EZP_CSPE_Render_Utility::get_display($config->linkedin_url, "inline-block"); ?>"><i class="fa fa-linkedin"></i></a>
                <a id="social-pinterest" target="_blank" href="<?php echo $config->pinterest_url ?>" style="background-color: #CC2127; display:<?php echo EZP_CSPE_Render_Utility::get_display($config->pinterest_url, "inline-block"); ?>"><i class="fa fa-pinterest"></i></a>
                <a id="social-soundcloud" target="_blank" href="<?php echo $config->soundcloud_url ?>" style="background-color: #f80; display:<?php echo EZP_CSPE_Render_Utility::get_display($config->soundcloud_url, "inline-block"); ?>"><i class="fa fa-soundcloud"></i></a>
                <a id="social-twitter" target="_blank" href="<?php echo $config->twitter_url ?>" style="background-color: #32CCFE;display:<?php echo EZP_CSPE_Render_Utility::get_display($config->twitter_url, "inline-block"); ?>"><i class="fa fa-twitter"></i></a>
                <a id="social-youtube" target="_blank" href="<?php echo $config->youtube_url ?>" style="background-color: #C52F30;display:<?php echo EZP_CSPE_Render_Utility::get_display($config->youtube_url, "inline-block"); ?>"><i class="fa fa-youtube"></i></a>                
            </div>

            <!-- Footer -->
            <footer class="text-center">
				
                <p id="footer" class="<?php echo $display->get_font_effect_class('text_footer'); ?>"><?php echo $content->footer; ?> </p> 
            </footer>          

        </div>

        <!-- Javascript files -->
        <!-- jQuery -->

        <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        
        <?php
         //   if($background_video)
			if($display->background_type == EZP_CSPE_Display_Background_Type::Video)
            {
        ?>
        <!-- Tubular Plugin -->
        <script  charset="utf-8" src="<?php echo $page_url . '/js/jquery.tubular.1.0.js?' . EZP_CSPE_Constants::PLUGIN_VERSION ?>"></script>
        <?php
            }
        ?>
        
        <!-- Countdown Plugin-->
        <script src="<?php echo $page_url . '/js/jquery.countdown.min.js' ?>"></script>
        
        <!-- Respond JS for IE8 -->
        <script src="<?php echo $page_url . '/js/respond.min.js?' . EZP_CSPE_Constants::PLUGIN_VERSION ?>"></script>
        <!-- HTML5 Support for IE -->
        <script src="<?php echo $page_url . '/js/html5shiv.js?' . EZP_CSPE_Constants::PLUGIN_VERSION ?>"></script>

        <script type="text/javascript">

    $('#countdown').countdown(clockEndDate, function (event) {

        var $this = $(this).html(event.strftime(''

                + '<div id="countdown-days" >%D <span><?php echo $content->countdown_days_text; ?></span></div> '
                + '<div id="countdown-hours" >%H <span><?php echo $content->countdown_hours_text; ?></span></div>  '
                + '<div id="countdown-minutes" >%M <span><?php echo $content->countdown_min_text; ?></span></div> '
                + '<div id="countdown-seconds" >%S <span><?php echo $content->countdown_sec_text; ?></span></div> '));
    });
    
    <?php
        //if ($background_video)
		if($display->background_type == EZP_CSPE_Display_Background_Type::Video)
        {
            ?>
            $().ready(function() {
                $('#youtube-player').tubular({mute:<?php echo $display->mute_background_video ? 'true' : 'false'; ?>, repeat: true, videoId: '<?php echo $display->youtube_video_id; ?>'}); // where idOfYourVideo is the YouTube ID.
            });
            <?php
        }
    ?>

         </script>
        </div>
		<?php echo $config->footer_scripts; ?>
    </body>	
</html>
