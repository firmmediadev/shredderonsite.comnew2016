<?php
/*
  Plugin Name: Coming Soon Page Elite
  Plugin URI: https://snapcreek.com/ezp-coming-soon/
  Description: Let people know that your site is coming soon or is being worked on. Visitors can submit their email addresses for future notification.
  Version: 2.1.2
  Author: Bob Riley
  Author URI: https://snapcreek.com/
  Text Domain: easy-pie-csp-elite
  License: GPL v3 
*/

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once("classes/class-ezp-cspe.php");

// require_once dirname(__FILE__) . '/debug.php'; //uncomments to turn on debug
define('EZP_CSPE_STORE_URL', 'https://snapcreek.com');
define('EZP_CSPE_ITEM_NAME', 'Coming Soon Page Elite');

//uncomment to force an update check set_site_transient('update_plugins', null);

if(!class_exists('EDD_SL_Plugin_Updater'))
{
    include (dirname(__FILE__) . '/lib/edd/EDD_SL_Plugin_Updater.php');
}

$license_key = get_option(EZP_CSPE_Constants::LICENSE_KEY_OPTION_NAME, '');

if(!empty($license_key))
{
	$ezp_cspe_edd_updater = new EDD_SL_Plugin_Updater(EZP_CSPE_STORE_URL, __FILE__,
                                         array('version' => EZP_CSPE_Constants::PLUGIN_VERSION, 'license' => $license_key, 'item_name' => EZP_CSPE_ITEM_NAME, 'author' => 'Snap Creek'));
}


$EZP_CSPE = new EZP_CSPE(__FILE__);

?>
