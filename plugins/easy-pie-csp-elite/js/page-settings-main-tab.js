/*
 * Code behind for settings / main tab
 */

jQuery(document).ready(function($) {
   
    $('#ezp-countdown-due-date').datetimepicker({ dateFormat: ezp_cs_datepicker_date_format} );
   
});