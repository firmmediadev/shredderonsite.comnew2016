<?php

require_once(EZP_CSPE_U::$PLUGIN_DIRECTORY . '/classes/Utilities/class-ezp-cspe-import-utility.php');
$export_nonce = wp_create_nonce('easy-pie-cspe-export-subscribers');
?>

<div class="wrap">

	<?php screen_icon(EZP_CSPE_Constants::PLUGIN_SLUG); ?>
	<?php
	if (isset($_GET['settings-updated']))
	{
		echo "<div class='updated'><p>" . EZP_CSPE_U::__('If you have a caching plugin, be sure to clear the cache!') . "</p></div>";
	}
	?>
    <div id="easypie-cs-options" class="inside">

		<?php
		$imported_text = null;
		$global = EZP_CSPE_Global_Entity::get_instance();
		$config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
		$error_string = "";

		if (isset($_POST['action']))
		{			
			check_admin_referer('easy-pie-csp-elite-data');
			if($_POST['action'] == 'import-lite-settings')
			{
				EZP_CSPE_Import_Utility::import_ezp_cs_settings();
				$imported_text = EZP_CSPE_U::__('Settings imported. <b>If you have a caching plugin be sure to clear it.</b>');
			}
			else if($_POST['action'] == 'import-lite-subscribers')
			{
				$subscriber_count = EZP_CSPE_Import_Utility::import_ezp_cs_subscribers();
				$imported_text = sprintf(EZP_CSPE_U::__('%d non-duplicate Subscribers imported. To push to subscriber list to mail providers, go to <a href="%s">Email Settings</a>.'), $subscriber_count, admin_url() . 'admin.php?page=' . EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=email-marketing');
			}
		}
		?>

		<?php wp_nonce_field('easy-pie-csp-elite-data'); ?>
        <input id="ezp-action" type="hidden" name="action" value="none"/>            
		<?php if ($error_string != "") : ?>
			<div id="message" class="error below-h2"><p><?php echo EZP_CSPE_U::__('Errors present:') . "<br/> $error_string" ?></p></div>
		<?php elseif ($imported_text != null) : ?>
			<div id="message" class="updated below-h2"><p><span><?php echo $imported_text; ?></span></p></div>
		<?php endif; ?>
			
		<!-- IMPORT -->
        <div class="postbox" style="margin-top:12px;" >
            <div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Import") ?></h3>		
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Settings") ?></th>
                        <td>
							<div class="compound-setting">                            
                                <button onclick="easyPie.Tools.Import.ConfirmAction('import-lite-settings', 'Current settings will be replaced with settings from Lite.'); return false;" <?php EZP_CSPE_U::echo_disabled(!EZP_CSPE_Import_Utility::is_ezp_cs_installed()) ?>>
									<?php EZP_CSPE_U::_e('Import'); ?>
								</button>
                                <div><span class="description"><?php EZP_CSPE_U::_e('Import settings from EZP Coming Soon Page Lite. Erases existing settings.'); ?></span></div>
                            </div>
							<br/><br/>
                        </td>
                    </tr>                          
					<tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Subscribers") ?></th>
                        <td>
							<div class="compound-setting">                            
                                <button onclick="easyPie.Tools.Import.SubmitAction('import-lite-subscribers'); return false;" <?php EZP_CSPE_U::echo_disabled(!EZP_CSPE_Import_Utility::is_ezp_cs_installed()) ?>>
									<?php EZP_CSPE_U::_e('Import'); ?>
								</button>
                                <div>
									<span class="description">
										<?php EZP_CSPE_U::_e("Import Lite subscribers to local subscriber list."); ?> <br/>
										<?php EZP_CSPE_U::_e("<b>Note:</b> Imported subscribers won't be auto-added to mail providers. Export list into CSV then import manually into providers." ); ?>
									</span>
								</div>
                            </div>
                        </td>
                    </tr> 
                </table>
				<br/><br/><br/>
				
				<!-- EXPORT -->
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Export") ?></h3>		
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Subscribers") ?></th>
                        <td>
							<div class="compound-setting">                            
                                <button onclick="location.href = ajaxurl + '?action=EZP_CSPE_export_all_subscribers&_wpnonce=<?php echo $export_nonce; ?>'; return false;" >
									<?php EZP_CSPE_U::_e('CSV Export'); ?>
								</button>
                                <div><span class="description"><?php EZP_CSPE_U::_e('Export subscriber list in CSV format.'); ?></span></div>
                            </div>

                        </td>
                    </tr>                          						
                </table>
            </div>
        </div>
		
</div>
</div>

<script>
jQuery(document).ready(function ($) {

	easyPie = {};
	easyPie.Tools = {};
	easyPie.Tools.Import = {};

	easyPie.Tools.Import.SubmitAction = function(action) {		
		$("#ezp-action").val(action);
		$("#easy-pie-cs-main-form").submit();
	}

	easyPie.Tools.Import.ConfirmAction = function(action, message) {

		if(confirm(message)) {
			easyPie.Tools.Import.SubmitAction(action);
		}
	}
});
</script>