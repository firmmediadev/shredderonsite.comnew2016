<div class="wrap">

    <?php screen_icon(EZP_CSPE_Constants::PLUGIN_SLUG); ?>
    <?php
    if (isset($_GET['settings-updated']))
    {
        echo "<div class='updated'><p>" . EZP_CSPE_U::__('If you have a caching plugin, be sure to clear the cache!') . "</p></div>";
    }
    ?>
    <div id="easypie-cs-options" class="inside">

        <?php
        $action_updated = null;
        $global = EZP_CSPE_Global_Entity::get_instance();
        $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
        $error_string = "";

        if (isset($_POST['action']) && $_POST['action'] == 'save')
        {
            check_admin_referer('easy-pie-csp-elite-save-settings');
            // Artificially set the bools since they aren't part of the postback
            $error_string = $config->set_post_variables($_POST);
            if ($error_string == "")
            {
                $config->fix_url_fields();
                $action_updated = $config->save();
            }
        }
        ?>

        <?php wp_nonce_field('easy-pie-csp-elite-save-settings'); ?>
        <input type="hidden" name="action" value="save"/>            
        <?php if ($error_string != "") :  ?>
            <div id="message" class="error below-h2"><p><?php echo EZP_CSPE_U::__('Errors present:') . "<br/> $error_string" ?></p></div>
        <?php endif; ?>

        <?php if ($action_updated) : ?>
            <div id="message" class="updated below-h2"><p><span><?php echo EZP_CSPE_U::__('Settings Saved.'); ?></span><strong style="margin-left:7px;"><?php echo '  ' . EZP_CSPE_U::__('If you have a caching plugin be sure to clear it.'); ?></strong></p></div>
        <?php endif; ?>
        
		<!-- ===================================
		FOLLOW URLs  -->
		<div class="postbox" >
			<div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Follow URLs") ?></h3>				
				
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Facebook") ?> </th>
                        <td>                         
                           <input class="long-input" name="facebook_url" type="text" value="<?php echo $config->facebook_url; ?>" />
						   <a href="http://facebook.com" target="_blank"><i class="fa fa-facebook-square"></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Google Plus") ?> </th>
                        <td>                          
                           <input class="long-input" name="google_plus_url" type="text" value="<?php echo $config->google_plus_url; ?>" />
						   <a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus-square"></i> </a>
						 
                        </td>                    
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Instagram") ?> </th>
                        <td>                         
                            <input class="long-input" name="instagram_url" type="text" value="<?php echo $config->instagram_url; ?>" />
							<a href="http://instagram.com" target="_blank"><i class="fa fa-instagram"></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("LinkedIn") ?> </th>
                        <td>                         
                            <input class="long-input" name="linkedin_url" type="text" value="<?php echo $config->linkedin_url; ?>" />
							<a href="http://linkedin.com" target="_blank"><i class="fa fa-linkedin-square"></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Pinterest") ?></th>
                        <td>                     
                            <input class="long-input" name="pinterest_url" type="text" value="<?php echo $config->pinterest_url; ?>" />
							<a href="http://pintrest.com" target="_blank"><i class="fa fa-pinterest-square"></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"> <?php echo EZP_CSPE_U::_e("SoundCloud") ?></th>
                        <td>                            
                            <input class="long-input" name="soundcloud_url" type="text" value="<?php echo $config->soundcloud_url; ?>" />
							<a href="http://soundcloud.com" target="_blank"><i class="fa fa-soundcloud"></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Twitter") ?></th>
                        <td>                      
							<input class="long-input" name="twitter_url" type="text" value="<?php echo $config->twitter_url; ?>" />
							<a href="http://twitter.com" target="_blank"><i class="fa fa-twitter-square"></i> </a>
                        </td>                    
                    </tr>                    
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("YouTube") ?></th>
                        <td>                        
                                <input class="long-input" name="youtube_url" type="text" value="<?php echo $config->youtube_url; ?>" />
								<a href="http://youtube.com" target="_blank"><i class="fa fa-youtube-square"></i> </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

