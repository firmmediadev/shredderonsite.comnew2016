
<?php
if (isset($_GET['tab']))
{
    $active_tab = $_GET['tab'];
}
else
{
    $active_tab = 'main';
}
?>

<?php if($active_tab == 'main'):  ?>
    <script type="text/javascript">
        ezp_cs_datepicker_date_format = "<?php echo EZP_CSPE_Render_Utility::get_datepicker_date_format(); ?>";       
    </script>
    
    <script>
        jQuery(document).ready(function($) {
            $('#ezp-countdown-due-date').datetimepicker({ timezone: <?php echo ((int)get_option('gmt_offset')) * 60 ?>, dateFormat: ezp_cs_datepicker_date_format} );
        });
    </script>
<?php endif; ?>
	
<style lang="text/css">
    .compound-setting { line-height:20px;}
    .narrow-input { width:66px;}
	.medium-input { width: 230px; }
    .long-input { width: 280px;}

	.postbox .inside {margin-bottom: 6px}
	.form-table th{padding: 8px 8px 8px 25px}
	.form-table td{padding: 3px 0 3px 0}
</style>

<div class="wrap">

    <?php screen_icon(EZP_CSPE_Constants::PLUGIN_SLUG); ?>
    <h2><?php EZP_CSPE_U::_e('Settings'); ?></h2>
    <?php
    if (isset($_GET['settings-updated']))
    {
        echo "<div class='updated'><p>" . EZP_CSPE_U::__('If you have a caching plugin, be sure to clear the cache!') . "</p></div>";
    }

    $global = EZP_CSPE_Global_Entity::get_instance();

    $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);

  //  EZP_CSPE_U::display_coming_soon_admin_notice($config->coming_soon_mode_on);
    ?>

    <div id="easypie-cs-options" class="inside">
        <h2 class="nav-tab-wrapper">  
            <a href="?page=<?php echo EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=main' ?>" class="nav-tab <?php echo $active_tab == 'main' ? 'nav-tab-active' : ''; ?>"><?php EZP_CSPE_U::_e('General'); ?></a>  
            <a href="?page=<?php echo EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=email-marketing' ?>" class="nav-tab <?php echo $active_tab == 'email-marketing' ? 'nav-tab-active' : ''; ?>"><?php EZP_CSPE_U::_e('Email'); ?></a>  
            <a href="?page=<?php echo EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=social' ?>" class="nav-tab <?php echo $active_tab == 'social' ? 'nav-tab-active' : ''; ?>"><?php EZP_CSPE_U::_e('Social'); ?></a>  
            <a href="?page=<?php echo EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=access' ?>" class="nav-tab <?php echo $active_tab == 'access' ? 'nav-tab-active' : ''; ?>"><?php EZP_CSPE_U::_e('Access'); ?></a>  
            <a href="?page=<?php echo EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=licensing' ?>" class="nav-tab <?php echo $active_tab == 'licensing' ? 'nav-tab-active' : ''; ?>"><?php EZP_CSPE_U::_e('Licensing'); ?></a>  
        </h2>
        
        <form name="easy_pie_cs_main_form" id="easy_pie_cs_main_form" method="post" action="<?php echo admin_url('admin.php?page=' . EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=' . $active_tab); ?>" > 
            <?php
            //  settings_fields(EZP_CSPE_Constants::MAIN_PAGE_KEY);
            //do_settings_sections(EZP_CSPE_Constants::MAIN_PAGE_KEY);                        
            ?>      
            <div id='tab-holder'>
                <?php
                if ($active_tab == 'main')
                {
                    include 'page-settings-main-tab.php';
                }
                else if ($active_tab == 'email-marketing')
                {
                    include 'page-settings-email-marketing-tab.php';
                }
                else if ($active_tab == 'social')
                {
                    include 'page-settings-social-tab.php';
                }
                else if($active_tab == 'access')
                {
                    include 'page-settings-access-tab.php';
                }
                else
                {
                    include 'page-settings-licensing-tab.php';
                }
                ?>         
                <!-- after redirect -->
            </div>           

            <input type="hidden" id="ezp-cspe-submit-type" name="ezp-cspe-submit-type" value="save"/>

            <p>
                <input type="submit" name="submit2" id="submit2" class="button button-primary" value="Save Changes" />
            </p>                

            <a href="https://snapcreek.com/ezp-coming-soon/docs/faqs-tech/" target="_blank"><?php EZP_CSPE_U::_e('FAQ'); ?></a>
            |
            <a href="https://snapcreek.com/support/" target="_blank"><?php EZP_CSPE_U::_e('Contact') ?></a>
        </form>
    </div>
</div>

