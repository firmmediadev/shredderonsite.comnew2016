<?php


if (isset($_GET['tab'])) 
{
    $active_tab = $_GET['tab'];
} else {
    $active_tab = 'display';
}

    $global = EZP_CSPE_Global_Entity::get_instance();
    $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
	$preview_mode = isset($_POST['settings-updated-preview']) && $_POST['settings-updated-preview'] == "true" ? true : false;
?>

<script type="text/javascript" src='<?php echo EZP_CSPE_U::$PLUGIN_URL . "/js/page-options-$active_tab-tab.js?" . EZP_CSPE_Constants::PLUGIN_VERSION; ?>'></script>

<style lang="text/css">
    .compound-setting { line-height:20px;}
    .narrow-input { width:66px;}
    .long-input { width: 345px;}
	
	.postbox .inside {margin-bottom: 6px}
	.form-table th{padding: 8px 8px 8px 25px}
	.form-table td{padding: 3px 0 3px 0}
</style>

<div class="wrap">
<form id="easy-pie-cs-main-form" method="post" action="<?php echo admin_url('admin.php?page=' . EZP_CSPE_Constants::PLUGIN_SLUG . '&tab=' . $active_tab); ?>" >  
	<input type="hidden" id="settings-updated-preview" name="settings-updated-preview" value="false" />
    <?php screen_icon(EZP_CSPE_Constants::PLUGIN_SLUG); ?>
    <h1><?php EZP_CSPE_U::_e('Template'); ?></h1>
    <?php
    if (isset($_GET['settings-updated'])) {
        echo "<div class='updated'><p>" . EZP_CSPE_U::__('If you have a caching plugin, be sure to clear the cache!') . "</p></div>";
    }
    ?>
    
    <div id="easypie-cs-options" class="inside">
        <h2 class="nav-tab-wrapper">  
            <a href="?page=<?php echo EZP_CSPE_Constants::PLUGIN_SLUG . '&tab=display' ?>" class="nav-tab <?php echo $active_tab == 'display' ? 'nav-tab-active' : ''; ?>">&nbsp; <?php EZP_CSPE_U::_e('Style'); ?> &nbsp;</a>  
            <a href="?page=<?php echo EZP_CSPE_Constants::PLUGIN_SLUG . '&tab=content' ?>" class="nav-tab <?php echo $active_tab == 'content' ? 'nav-tab-active' : ''; ?>">&nbsp; <?php EZP_CSPE_U::_e('Text'); ?> &nbsp;</a>  
			
			<button class="button button-primary button-large" style="float: right" onclick="easyPie.CSPE.Options.Preview()" >
				<?php EZP_CSPE_U::_e('View Page'); ?> <i class="fa fa-external-link-square" aria-hidden="true"></i>
			</button> 
			<input type="submit" name="submit"  class="button button-primary button-large" value="<?php EZP_CSPE_U::_e('Update'); ?>" style="float:right; margin-right: 5px" /> 
        </h2>
        
            <div id='tab-holder'>
                <?php
                if ($active_tab == 'display') {
                    include 'page-options-display-tab.php';
                } else if ($active_tab == 'content') {
                    include 'page-options-content-tab.php';
				}
                ?>         
            </div>           

            <input type="hidden" id="ezp-cspe-submit-type" name="ezp-cspe-submit-type" value="save"/>
            
            <p>
				 <input type="submit" name="submit"  class="button button-large button-primary" value="<?php EZP_CSPE_U::_e('Update'); ?>" /> &nbsp; 
				 <button class="button button-primary button-large" onclick="easyPie.CSPE.Options.Preview()" >
					<?php EZP_CSPE_U::_e('View Page'); ?> <i class="fa fa-external-link-square" aria-hidden="true"></i>
				 </button> 
            </p>   
			<br/>

            <a href="https://snapcreek.com/ezp-coming-soon/docs/faqs-tech/" target="_blank"><?php EZP_CSPE_U::_e('FAQ'); ?></a> |           
            <a href="https://snapcreek.com/support/" target="_blank"><?php EZP_CSPE_U::_e('Contact') ?></a>
    </div>
</form>	
</div>
<script>
	easyPie.CSPE = {};
	easyPie.CSPE.Options = {};

    jQuery(document).ready(function ($) 
	{
		easyPie.CSPE.Options.Preview = function() 
		{
			$('#settings-updated-preview').val('true');
		}
		<?php 
			  if ($preview_mode) 
			  {
				$preview_url = get_site_url() . '?EZP_CSPE_preview=true' ; 
				echo "window.open('{$preview_url}').focus();";
			  }
		?>
    });
</script>
