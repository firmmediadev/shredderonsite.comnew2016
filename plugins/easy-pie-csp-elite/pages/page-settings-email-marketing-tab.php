<?php
require_once(dirname(__FILE__) . '/../classes/Utilities/class-ezp-cspe-utility.php');
require_once(dirname(__FILE__) . '/../classes/Utilities/class-ezp-cspe-email-utility.php');
require_once(dirname(__FILE__) . '/../classes/Utilities/class-ezp-cspe-mailchimp.php');

if(EZP_CSPE_U::PHP53())
{
	require_once(dirname(__FILE__) . '/../classes/Utilities/class-ezp-cspe-constant-contact.php');
	require_once(dirname(__FILE__) . '/../lib/Ctct/ConstantContact-Config.php');
}

// use Ctct\Auth\CtctOAuth2;
// use Ctct\Exceptions\OAuth2Exception;
?>

<div class="wrap">
    <?php screen_icon(EZP_CSPE_Constants::PLUGIN_SLUG); ?>
    <div id="easypie-cs-options" class="inside">

        <?php
		$action_text = "";
        $global = EZP_CSPE_Global_Entity::get_instance();
        $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
        $error_string = "";

        if (isset($_POST['action']))
        {
			check_admin_referer('easy-pie-csp-elite-save-settings');
			if($_POST['action'] == 'save')
			{				
				// Artificially set the bools since they aren't part of the postback            
                $config->mailchimp_enabled = isset($_POST['mailchimp_enabled']);
                $config->mailchimp_double_optin = isset($_POST['mailchimp_double_optin']);
                $config->send_email_on_subscribe = isset($_REQUEST['send_email_on_subscribe']);

                if($config->mailchimp_enabled === false)
                {
                    $config->mailchimp_list_name = '';
                    $config->mailchimp_list_id = -1;
                }

				$config->constant_contact_enabled = isset($_POST['constant_contact_enabled']);
                $config->constant_contact_access_token = isset($_POST['constant_contact_access_token']);

				if($config->constant_contact_enabled === false)
				{
					$config->constant_contact_list_name = '';
					$config->constant_contact_list_id = -1;
				}
				$error_string = $config->set_post_variables($_POST);

				if ($error_string == "")
				{
					$config->fix_url_fields();
					$action_updated = $config->save();
					if($action_updated)
					{
						$action_text = EZP_CSPE_U::__('Settings Saved');
					}
				}
			}
			else if($_POST['action'] == "push-mailchimp")
			{
				$error_list = EZP_CSPE_Email_Utility::push_subscriber_list_to_mail_providers(EZP_CSPE_Mail_Provider_Type::MailChimp);
				// RSR TODO: Push all subscribers to mailchimp
				$action_text = EZP_CSPE_U::__('Pushed subscribers to MailChimp.');
				if(count($error_list) > 0)
				{
					$action_text .= EZP_CSPE_U::__(' One or more errors present however. Please check error log.');	
				}							
			}
        }

        $mailchimp_not_configured_text = EZP_CSPE_U::__('Not Configured');
        $mailchimp_list_disabled = false;
        $mailchimp_lists = false;
        
        if ($config->mailchimp_enabled == false)
        {
            $mailchimp_list_disabled = true;
        }
        else
        {
            $mailchimp = new EZP_CSPE_MailChimp($config->mailchimp_apikey);
            $mailchimp_lists = $mailchimp->get_lists();

            if ($mailchimp_lists === false)
            {
                $mailchimp_not_configured_text = EZP_CSPE_U::__('Error Retrieving');
                $mailchimp_list_disabled = true;
            }
        }
		
		if(EZP_CSPE_U::PHP53())
		{
			$constant_contact_not_configured_text = EZP_CSPE_U::__('Not Configured');
			$constant_contact_list_disabled = false;
			$constant_contact_lists = false;

			$redirect  = admin_url('admin.php?page=easy-pie-csp-elite-settings&tab=email-marketing');
			$oauth_url = ( CTCT_REDIRECT_URI . '?redirect=' . $redirect );

			//$oauth = new Ctct\Auth\CtctOAuth2(CTCT_APIKEY, CTCT_CONSUMER_SECRET, $oauth_url);
			$oauth = EZP_CSPE_Constant_Contact::create_oath2(CTCT_APIKEY, CTCT_CONSUMER_SECRET, $oauth_url);

			if (isset($_GET['code'])) 
			{
				try 
				{
					$accessToken = $oauth->getAccessToken($_GET['code']);
					/* sample
					array(3) {
					  ["access_token"]=>
					  string(36) "0aecd851-gyf7-4612-a0da-0f6a90cfb241"
					  ["expires_in"]=>
					  int(315275887)
					  ["token_type"]=>
					  string(6) "Bearer"
					}
					*/
					$config->constant_contact_access_token=$accessToken['access_token'];
					EZP_CSPE_U::log_object('accessToken from code', $accessToken);
				} 
				catch (Exception $ex) 
				{
					EZP_CSPE_U::log_object('Something when wrong when try to get accessToken from code', $ex);
				}
			}

			if ($config->constant_contact_enabled == false)
			{
				$constant_contact_list_disabled = true;
			}
			else
			{
				$constant_contact = new EZP_CSPE_Constant_Contact(CTCT_APIKEY, $config->constant_contact_access_token);
				$constant_contact_lists = $constant_contact->get_lists();

				if ($constant_contact_lists == false)
				{
					$constant_contact_not_configured_text = EZP_CSPE_U::__('Error Retrieving');
					$constant_contact_list_disabled = true;
				}
			}
		}
        ?>

        <?php wp_nonce_field('easy-pie-csp-elite-save-settings'); ?>
        <input id="ezp-action" type="hidden" name="action" value="save"/>            
        <?php if ($error_string != "") : ?>
            <div id="message" class="error below-h2"><p><?php echo EZP_CSPE_U::__('Errors present:') . "<br/> $error_string" ?></p></div>
        <?php endif; ?>

        <?php if ($action_text != null) : ?>
            <div id="message" class="updated below-h2"><p><span><?php echo $action_text; ?></span></p></div>
        <?php endif; ?>


		<!-- ===================================
		ALERTS  -->
		<div class="postbox" >
			<div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Alerts") ?></h3>
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Enabled") ?></th>
                        <td>                       
							<input type="checkbox" id="send_email_on_subscribe" name="send_email_on_subscribe" <?php echo $config->send_email_on_subscribe ? 'checked' : ''; ?> />                                                                                                                
							<span><?php EZP_CSPE_U::_e("Yes") ?></span>                                
							<p class="description"><?php EZP_CSPE_U::_e('Receive email when someone subscribes.'); ?></p>
                        </td>
                    </tr>                                            
					<tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Email Address") ?></th>
                        <td>                         
							<input class="medium-input" id="alerts_email_address" name="alerts_email_address" type="text" value="<?php echo $config->alerts_email_address; ?>" />
							<p class="description"><?php EZP_CSPE_U::_e('Where to send alerts. Leave blank for admin email.'); ?></p>
                        </td>                            
                    </tr>
                </table>
            </div>
        </div>

		<!-- ===================================
		MAILCHIMP  -->
		<div class="postbox" >
			<div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("MailChimp") ?></h3>
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Enabled") ?></th>
                        <td>                    
							<input onclick="easyPie.Settings.EmailMarketing.SetDisabledStates();" type="checkbox" id="mailchimp_enabled" name="mailchimp_enabled" <?php echo $config->mailchimp_enabled ? 'checked' : ''; ?> />                                                                                                                
							<span><?php EZP_CSPE_U::_e("Yes") ?></span>
                        </td>
                    </tr>   
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("API Key") ?></th>
                        <td>                        
                            <input class="mailchimp-setting long-input" id="mailchimp_apikey" name="mailchimp_apikey" type="text" value="<?php echo $config->mailchimp_apikey; ?>" />
                        </td>                            
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("List") ?></th>
                        <td>
                            <select class="mailchimp-setting" <?php echo ($mailchimp_list_disabled ? 'disabled' : ''); ?> name="mailchimp_list_id" id="mailchimp_list_id">
                                <?php
                                                        
                                $mailchimp_not_configured_selected = (($config->mailchimp_list_id == -1) ? 'selected' : '');
                                
                                echo "<option value='-1' $mailchimp_not_configured_selected>$mailchimp_not_configured_text</option>";
                                
                                if($mailchimp_lists !== false)
                                {
                                    foreach ($mailchimp_lists as $mailchimp_list)
                                    {
                                        $id = $mailchimp_list['id'];
                                        $name = $mailchimp_list['name'];
                                        $selected_string = ($config->mailchimp_list_id == $id ? 'selected' : '');
                                        echo "<option $selected_string value='$id'>$name</option>";
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>     

                    <tr>
                        <th scope="row"><?php EZP_CSPE_U::_e("Double Opt-in") ?> </th>
                        <td>                        
							<input class="mailchimp-setting" type="checkbox" id="mailchimp_double_optin" name="mailchimp_double_optin" <?php echo $config->mailchimp_double_optin ? 'checked' : ''; ?> />                                                                                                                
							<span><?php EZP_CSPE_U::_e("Yes") ?></span>
                        </td>
                    </tr>   
<!--                    <tr>
                        <th scope="row">
                            <?php EZP_CSPE_U::_e('Subscribers'); ?>
                        </th>
                        <td>
                            <button onclick="easyPie.Settings.EmailMarketing.SetPushSubscribersAction('push-mailchimp'); return false;"><?php EZP_CSPE_U::_e('Push'); ?></button>
                            <p class="description"><?php EZP_CSPE_U::_e("Send subscribers to MailChimp. Use after importing Lite subscribers. Don't use for large subscribers lists - use CSV export instead."); ?></p>
                        </td>    
                    </tr>-->
                        
                </table>
            </div>
        </div>
        <!-- ===================================
        Constant Contact  -->
        <div class="postbox" >
            <div class="inside" >				
                <h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Constant Contact") ?></h3>
				<?php if(EZP_CSPE_U::PHP53()): ?>
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Enabled") ?></th>
                        <td>                    
                            <input onclick="easyPie.Settings.EmailMarketing.ConstantContact.SetDisabledStates();" type="checkbox" id="constant_contact_enabled" name="constant_contact_enabled" <?php echo $config->constant_contact_enabled ? 'checked' : ''; ?> />                                                                                                                
                            <span><?php EZP_CSPE_U::_e("Yes") ?></span>
                        </td>
                    </tr>                   
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Access Token") ?></th>
                        <td>
							<input class="constant_contact-setting long-input" id="constant_contact_access_token" name="constant_contact_access_token" type="text" value="<?php echo $config->constant_contact_access_token; ?>" />
                            <a target="_blank" class="button button-primary" href="<?php echo $oauth->getAuthorizationUrl(); ?>">Get</a>
							<p class="description"><?php EZP_CSPE_U::_e("Click 'Get' to login to Constant Contact and give access permission. Afterward you'll get directed back to this page where you should click 'Save' to store the token."); ?></p>
                        </td>                            
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("List") ?></th>
                        <td>
                            <select class="constant_contact-setting" <?php echo ($constant_contact_list_disabled ? 'disabled' : ''); ?> name="constant_contact_list_id" id="constant_contact_list_id">
                                <?php
                                                        
                                $constant_contact_not_configured_selected = (($config->constant_contact_list_id == -1) ? 'selected' : '');
                                
                                echo "<option value='-1' $constant_contact_not_configured_selected>$constant_contact_not_configured_text</option>";
                                
                                if($constant_contact_lists !== false)
                                {
                                    foreach ($constant_contact_lists as $constant_contact_list)
                                    {
                                        $id = $constant_contact_list['id'];
                                        $name = $constant_contact_list['name'];
                                        $selected_string = ($config->constant_contact_list_id == $id ? 'selected' : '');
                                        echo "<option $selected_string value='$id'>$name</option>";
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>     
                </table>
            </div>		
			<?php else: ?>
			<p>
				<?php EZP_CSPE_U::_e('Constant Contact support disabled. (requires PHP 5.3 or greater)'); ?>
			</p>
			<?php endif; ?>
        </div> <!-- end of constant contact -->
    </div>
</div>

<script>
jQuery(document).ready(function ($) {

	easyPie = {};
	easyPie.Settings = {};
	easyPie.Settings.EmailMarketing = {};

	easyPie.Settings.EmailMarketing.SetDisabledStates = function () {

		var mc_checked = $('#mailchimp_enabled').prop('checked');

		$('.mailchimp-setting').prop('disabled', (mc_checked == false));
	}

	$("form").submit(function () {
		$("input").removeAttr("disabled");
	});

	easyPie.Settings.EmailMarketing.SetPushSubscribersAction = function(action) {

		$("#ezp-action").val(action);
		$("#easy_pie_cs_main_form").submit();
	}

	easyPie.Settings.EmailMarketing.SetDisabledStates();

    easyPie.Settings.EmailMarketing.ConstantContact = {};
    easyPie.Settings.EmailMarketing.ConstantContact.SetDisabledStates = function () {

        var mc_checked = $('#constant_contact_enabled').prop('checked');

        $('.constant_contact-setting').prop('disabled', (mc_checked == false));
    }
    easyPie.Settings.EmailMarketing.ConstantContact.SetDisabledStates();
});
</script>