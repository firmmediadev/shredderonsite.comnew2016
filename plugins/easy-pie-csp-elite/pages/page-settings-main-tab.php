<div class="wrap">

    <?php screen_icon(EZP_CSPE_Constants::PLUGIN_SLUG); ?>
    <?php
    if (isset($_GET['settings-updated']))
    {
        echo "<div class='updated'><p>" . EZP_CSPE_U::__('If you have a caching plugin, be sure to clear the cache!') . "</p></div>";
    }
    ?>
    <div id="easypie-cs-options" class="inside">
        <?php
        $action_updated = null;
        $global = EZP_CSPE_Global_Entity::get_instance();
        $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
        $error_string = "";

        if (isset($_POST['action']) && $_POST['action'] == 'save')
        {
            check_admin_referer('easy-pie-csp-elite-save-settings');
            // Artificially set the bools since they aren't part of the postback
            $config->collect_email = false;
            $config->collect_name = false;
            $config->disable_after_countdown = false;
            $error_string = $config->set_post_variables($_POST);

            if ($error_string == "")
            {
                $config->fix_url_fields();
                $action_updated = $config->save();
            }
        }
        ?>

        <?php wp_nonce_field('easy-pie-csp-elite-save-settings'); ?>
        <input type="hidden" name="action" value="save"/>            
        <?php  if ($error_string != "") :  ?>
            <div id="message" class="error below-h2"><p><?php echo EZP_CSPE_U::__('Errors present:') . "<br/> $error_string" ?></p></div>
        <?php endif; ?>

        <?php if ($action_updated) : ?>
            <div id="message" class="updated below-h2"><p><span><?php echo EZP_CSPE_U::__('Settings Saved.'); ?></span><strong style="margin-left:7px;"><?php echo '  ' . EZP_CSPE_U::__('If you have a caching plugin be sure to clear it.'); ?></strong></p></div>
        <?php endif; ?>

        <div class="postbox" style="margin-top:12px;" >
            <div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Basic") ?></h3>
				
				<!-- GENERAL -->
				<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("GENERAL") ?></div>	
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Status") ?></th>
                        <td>                       
							<input type="radio" name="coming_soon_mode_on" value="true" <?php echo $config->coming_soon_mode_on ? 'checked' : ''; ?>/><span><?php echo EZP_CSPE_U::__('On'); ?></span>
							<input type="radio" name="coming_soon_mode_on" value="" <?php echo $config->coming_soon_mode_on ? '' : 'checked'; ?>/><span><?php echo EZP_CSPE_U::__('Off'); ?></span>                                    
                        </td>
                    </tr>                          
                </table>

				
				<!-- COLLECTION -->
				<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("COLLECTION") ?></div>	
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Collect Email") ?></th>
                        <td>                        
							<input type="checkbox" name="collect_email" <?php echo $config->collect_email ? 'checked' : ''; ?> />                                                                                                                
							<span><?php EZP_CSPE_U::_e("Yes") ?></span>
                        </td>
                    </tr>   
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Collect Name") ?></th>
                        <td>                         
							<input type="checkbox" name="collect_name" <?php echo $config->collect_name ? 'checked' : ''; ?> />                                                                                                                
							<span><?php EZP_CSPE_U::_e("Yes") ?></span>
                        </td>
                    </tr>   
                </table>

				
                <!-- COUNTDOWN -->
				<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("Countdown") ?></div>	
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Date") ?></th>
                        <td>                    
							<input style="width:130px;" id="ezp-countdown-due-date" class="long-input" name="countdown_due_date" type="text" value="<?php EZP_CSPE_U::_he($config->countdown_due_date); ?>" />
							<div><span class="description"><?php EZP_CSPE_U::_e('Countdown timer will display when populated. Make sure WordPress timezone is correct.'); ?></span></div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Auto-Off") ?></th>
                        <td>                        
							<input type="checkbox" name="disable_after_countdown" <?php echo $config->disable_after_countdown ? 'checked' : ''; ?> />                                                                                                                
							<span><?php EZP_CSPE_U::_e("Yes") ?></span>
							<div><span class="description"><?php EZP_CSPE_U::_e('Turn off coming soon page when countdown hits. Disable caching plugins to ensure turns off at correct time.'); ?></span></div>
                        </td>
                    </tr> 
                </table>
            </div></div>   

   
		<!-- ===================================
		ADVANCED  -->
		<div class="postbox" >
			<div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Advanced") ?></h3>
				
				<!-- HTTP -->
				<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("HTTP") ?></div>	
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Return Code") ?></th>
                        <td>                       
							<input type="radio" name="return_code" value="200" <?php echo $config->return_code == 200 ? 'checked' : ''; ?> /><?php echo EZP_CSPE_U::_e("200") ?>
							<input type="radio" name="return_code" value="503" <?php echo $config->return_code == 503 ? 'checked' : ''; ?> /><?php echo EZP_CSPE_U::_e("503") ?>
							<div><span class="description"><?php EZP_CSPE_U::_e('200 tells search engines to index the page while 503 means the site is temporarily offline and not to index.'); ?></span></div>
                        </td>
                    </tr>  
                </table>
				<br/>
				
				<!-- SEO -->
				<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("SEO") ?></div>	
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Author URL") ?></th>
                        <td>                      
							<input class="long-input" name="author_url" type="text" value="<?php echo $config->author_url; ?>" />
							<div><span class="description"><?php EZP_CSPE_U::_e('Google+ or other identifying URL'); ?></span></div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Meta Description") ?></th>
                        <td>                        
							<textarea rows="5" cols="60" name="meta_description"><?php echo EZP_CSPE_U::_he($config->meta_description); ?></textarea>
                        </td>
                    </tr>    
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Meta Keywords") ?></th>
                        <td>                          
							<input class="long-input" name="meta_keywords" type="text" value="<?php echo $config->meta_keywords; ?>" />
							<div><span class="description"><?php EZP_CSPE_U::_e('Comma separated list'); ?></span></div>
                        </td>
                    </tr>  
				</table>
				<br/>
			
				<!-- SCRIPTING -->
				<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("SCRIPTING") ?></div>	
				<table class="form-table"> 
					<tr>
						<th scope="row"><?php echo EZP_CSPE_U::_e("Header Scripts") ?></th>
						<td>
							<textarea rows="5" cols="60" name="analytics_code"><?php echo EZP_CSPE_U::_he($config->analytics_code); ?></textarea>                        
							<div><span class="description"><?php echo EZP_CSPE_U::__('Scripts insert before closing head tag, including some analytics tracking codes') . ' (' . EZP_CSPE_U::__('include') . '&lt;script&gt;&lt;/script&gt;)'; ?></span></div>
						</td>
					</tr>
					<tr>
						<th scope="row"><?php echo EZP_CSPE_U::_e("Footer Scripts") ?></th>
						<td>                          
							<textarea rows="5" cols="60" name="footer_scripts"><?php echo EZP_CSPE_U::_he($config->footer_scripts); ?></textarea>                        
							<div><span class="description"><?php echo EZP_CSPE_U::__('Scripts insert before closing body tag, including some analytics tracking codes') . ' (' . EZP_CSPE_U::__('include') . '&lt;script&gt;&lt;/script&gt;)'; ?></span></div>
						</td>
					</tr>
				</table>
			</div>
		</div>        

	
</div>
</div>

