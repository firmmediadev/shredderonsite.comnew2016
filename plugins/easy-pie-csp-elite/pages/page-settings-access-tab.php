<?php
$__dir__ = dirname(__FILE__);
$action_updated = null;
$global = EZP_CSPE_Global_Entity::get_instance();
$config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
$error_string = "";

include($__dir__ . '/../classes/Entities/class-ezp-cspe-access-key-entity.php');

if (isset($_REQUEST['action']))
{
    if ($_REQUEST['action'] == 'access_key_edit')
    {
        $access_key_id = (int) $_REQUEST['access_key_id'];
        $access_key_name = $_REQUEST['access_key_name'];
        // $access_key_expiration = $_REQUEST['access_key_expiration'];

        if ($access_key_id == -1)
        {
            $access_key = new EZP_CSPE_Access_Key_Entity();
        }
        else
        {
            $access_key = EZP_CSPE_Access_Key_Entity::get_by_id($access_key_id);
        }

        $access_key->name = $access_key_name;
        // $access_key->expiration_date = $access_key_expiration;
        $access_key->save();
    }
    else if ($_REQUEST['action'] == 'access_key_delete')
    {
        $access_key_id = (int) $_REQUEST['access_key_id'];
        $access_key = EZP_CSPE_Access_Key_Entity::get_by_id($access_key_id);

        if ($access_key != null)
        {
            $access_key->delete();
        }
    }
    else if($_REQUEST['action'] == 'save')
    {
        check_admin_referer('easy-pie-csp-elite-save-settings-access');   
        $error_string = $config->set_post_variables($_REQUEST);

        if ($error_string == "")
        {
            $action_updated = $config->save();
        }
    }
}
?>

<style type="text/css">
    div.sub-menu {margin: 5px 0 0 2px; display: none}
</style>

<input form="easy_pie_cs_main_form" id="action" name="action" type="hidden" value="save" />

<div class="wrap">

    <?php screen_icon(EZP_CSPE_Constants::PLUGIN_SLUG); ?>
    <?php
    if (isset($_GET['settings-updated']))
    {
        echo "<div class='updated'><p>" . EZP_CSPE_U::__('If you have a caching plugin, be sure to clear the cache!') . "</p></div>";
    }

    $access_keys = EZP_CSPE_Access_Key_Entity::get_all();
    $access_key_count = count($access_keys);
    ?>
    <div id="easypie-cs-options" class="inside">

        <?php wp_nonce_field('easy-pie-csp-elite-save-settings-access'); ?>        

        <?php
        if ($error_string != "") :
            ?>
            <div id="message" class="error below-h2"><p><?php echo EZP_CSPE_U::__('Errors present:') . "<br/> $error_string" ?></p></div>
        <?php endif; ?>

        <?php if ($action_updated) : ?>
            <div id="message" class="updated below-h2"><p><span><?php echo EZP_CSPE_U::__('Settings Saved.'); ?></span><strong style="margin-left:7px;"><?php echo '  ' . EZP_CSPE_U::__('If you have a caching plugin be sure to clear it.'); ?></strong></p></div>
        <?php endif; ?>
            
	
		<!-- ===================================
		ACCESS KEYS  -->
		<div class="postbox" >
			<div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Access Keys") ?></h3>	
				<br/>
                <table class="widefat storage-tbl">
                    <thead>
                        <tr>
                            <th style='width:140px;'><?php EZP_CSPE_U::_e('Name'); ?></th>
                            <th style="width:675px;"><?php EZP_CSPE_U::_e('Link'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($access_keys as $access_key) :
                            /* @var $access_key EZP_CSPE_Access_Key_Entity */
                            $i++;
                            ?>
                            <tr id='main-view-<?php echo $access_key->id ?>' class="access-key-row <?php echo ($i % 2) ? 'alternate' : ''; ?>">
                                <td>
                                    <a href="javascript:void(0);" onclick="EZP_CSPE.AccessKey.Edit(<?php echo "$access_key->id, '$access_key->name', '$access_key->key'"; ?>);"><b><?php echo $access_key->name; ?></b></a>
                                    <div class="sub-menu">
                                        <a href="javascript:void(0);" onclick="EZP_CSPE.AccessKey.Edit(<?php echo "$access_key->id, '$access_key->name', '$access_key->key'"; ?>);">Edit</a> |
                                        <a href="javascript:void(0);" onclick="EZP_CSPE.AccessKey.Delete(<?php echo "$access_key->id, '$access_key->name'"; ?>);
                                                return false;">Delete</a>
                                    </div>          
                                </td>
                                <td><span onclick="EZP_CSPE.AccessKey.Select(this);"><?php echo $access_key->get_link() ?></span></td>
                            </tr>                
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>
                                <button id="EZP_CSPE_add_new_access_key" class="button button-secondary" onclick="EZP_CSPE.AccessKey.AddNew();
                                        return false;"><?php EZP_CSPE_U::_e('Add'); ?></button>
                            </th>
                            <th  style="text-align:right; font-size:12px">						
                                Total: <?php echo $access_key_count ?>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <p style="margin-left:13px;" class="description"><?php EZP_CSPE_U::_e('Links create cookie which gives people access to your <strong>entire</strong> site. Delete to revoke access.'); ?></p>
        </div>

		<!-- ===================================
		FILTERS  -->
		<div class="postbox" >
			<div class="inside" >
				<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Filters") ?></h3>	
				
                <table class="form-table"> 
                    <tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Unfiltered URLs") ?></th>
                        <td>                        
                            <textarea rows="5" cols="60" name="unfiltered_urls" ><?php echo $config->unfiltered_urls; ?></textarea>
                            <div><span class="description"><?php EZP_CSPE_U::_e('Each line contains a relative URL <strong>all visitors</strong> can access (Note: /path allows access to /path, /path/1 and /path/2)'); ?></span></div>
                        </td>
                    </tr>                           
					<tr>
                        <th scope="row"><?php echo EZP_CSPE_U::_e("Allowed IPs") ?></th>
                        <td>
                            <textarea rows="5" cols="60" name="allowed_ips"><?php echo $config->allowed_ips; ?></textarea>
                            <div><span class="description"><?php EZP_CSPE_U::_e('Each line contains an IP address of a visitor is allowed to see the site when Coming Soon Page is engaged.'); ?></span></div>
                        </td>
                    </tr>                           
                </table>
            </div>
        </div>    
    </div>    

    <div style="display:none" id="add_edit_access_key" title="<?php EZP_CSPE_U::_e('Add/Edit Access Key'); ?>">
        <input form="easy_pie_cs_main_form" name="access_key_id" type="hidden" id="access_key_id" value="-1" />        

        <br/>
        <label style="width:90px;display:inline-block" for="access_key_name" >Name</label>
        <input form="easy_pie_cs_main_form" name="access_key_name" id="access_key_name" />
        <br/>

        <!--        <label style="width:90px;display:inline-block" for="access_key_name" >Expiration</label>        
                <input readonly form="easy_pie_cs_main_form" name="access_key_expiration" id="access_key_expiration" />       -->

        <br/><br/>
        <div style="text-align:center;">
            <input type="submit" form="easy_pie_cs_main_form" class="button button-primary" value="Save">
            <input type="button" onclick="jQuery('#add_edit_access_key').dialog('close');" class="button button-secondary" value="Cancel" />
        </div>
    </div>    
</div>

<script>
    jQuery(document).ready(function ($) {

        EZP_CSPE = new Object();
        EZP_CSPE.AccessKey = new Object();
        EZP_CSPE.AccessKey.Delete = function (id, name) {

            if (confirm("<?php EZP_CSPE_U::_e('Delete'); ?>" + " " + name + "?"))
            {
                $('#action').val('access_key_delete')
                $("#access_key_id").val(id);
                EZP_CSPE.AccessKey.Submit();
            }
        };

        EZP_CSPE.AccessKey.Edit = function (id, name, key) {

            if (id != -1)
            {
                title = 'Edit '.name;
            }
            else
            {
                title = 'Add New';
            }

            $('#action').val('access_key_edit')
            $('#add_edit_access_key').attr('title', title);
            $("#access_key_id").val(id);
            $('#access_key_name').val(name);
            $('#add_edit_access_key').dialog({position: {at: 'right+400 top-50', of: '#EZP_CSPE_add_new_access_key'}, width: 350});
        };

        EZP_CSPE.AccessKey.AddNew = function () {

            EZP_CSPE.AccessKey.Edit(-1, '', '', '');
        }

        EZP_CSPE.AccessKey.Submit = function () {

            document.forms['easy_pie_cs_main_form'].submit();
        }

        EZP_CSPE.AccessKey.Select = function (that) {

            var range, selection;

            if (window.getSelection && document.createRange) {
                selection = window.getSelection();
                range = document.createRange();
                range.selectNodeContents($(that)[0]);
                selection.removeAllRanges();
                selection.addRange(range);
            } else if (document.selection && document.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText($(that)[0]);
                range.select();
            }
        }
        
        $("tr.access-key-row").hover(
                function () {
                    $(this).find(".sub-menu").show();
                },
                function () {
                    $(this).find(".sub-menu").hide();
                }
        );

        $('#access_key_name').keypress(function (e) {
            if (e.which == 13) {
                document.forms['easy_pie_cs_main_form'].submit();
                return false;    //<---- Add this line
            }
        });
    });
</script>

