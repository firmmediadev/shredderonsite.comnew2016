<?php
/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
?>

<style>
    #submit2 { display:none;}
</style>
    

<?php
global $wp_version;
global $wpdb;


require_once(dirname(__FILE__) . '/../classes/Utilities/class-ezp-cspe-license-utility.php');

$force_refresh = true;
$nonce_action = 'ezp-cspe-settings-licensing-edit';

$action_updated = null;


$global = EZP_CSPE_Global_Entity::get_instance();

//SAVE RESULTS
if (isset($_REQUEST['action']))
{    
    check_admin_referer($nonce_action);
    $action = $_REQUEST['action'];
    
    switch($action)
    {
        case 'save':      
        case 'activate':
            $submitted_license_key = trim($_REQUEST['_license_key']);
            update_option(EZP_CSPE_Constants::LICENSE_KEY_OPTION_NAME, $submitted_license_key);

            $action_updated = EZP_CSPE_License_Utility::change_license_activation(true);
            if($action_updated)
            {
                $action_response = EZP_CSPE_U::__("License Activated");    
            }
            else
            {
                $action_response = EZP_CSPE_U::__("Error Activating License");    
            }
            
            break;
                
        case 'deactivate':
            $action_updated = EZP_CSPE_License_Utility::change_license_activation(false);
            if($action_updated)
            {
                $action_response = EZP_CSPE_U::__("License Deactivated");    
            }
            else
            {
                $action_response = EZP_CSPE_U::__("Error Deactivating License");    
            }
            break;
     }
     
     $force_refresh = true;
}

$license_status = EZP_CSPE_License_Utility::get_license_status($force_refresh);
$activate_button_text = EZP_CSPE_U::__('Activate');     

if($license_status == EZP_CSPE_License_Status::Valid)
{
    $license_status_style = 'color:#509B18';
    $license_status_text = EZP_CSPE_U::__('Status: Active');
    $activate_button_text = EZP_CSPE_U::__('Deactivate');
}
else if(($license_status == EZP_CSPE_License_Status::Inactive) || ($license_status == EZP_CSPE_License_Status::Site_Inactive))
{
    $license_status_style = 'color:#dd3d36;';
    $license_status_text = EZP_CSPE_U::__('Status: Inactive');
}
else if($license_status == EZP_CSPE_License_Status::Expired)
{
	$license_key = get_option(EZP_CSPE_Constants::LICENSE_KEY_OPTION_NAME, '');				
	$renewal_url = 'https://snapcreek.com/checkout?edd_license_key=' . $license_key;				
	$license_status_style = 'color:#dd3d36;';    
	$license_status_text = sprintf('Your Coming Soon Elite license key has expired so you aren\'t getting important updates! <a target="_blank" href="%1$s">Renew your license now</a>', $renewal_url);
}
else
{
    $license_status_string = EZP_CSPE_License_Utility::get_license_status_string($license_status);
    $license_status_style = 'color:#dd3d36;';

    $license_status_text = sprintf(EZP_CSPE_U::__('Status: %1$s. Please %2$sgo to snapcreek.com%3$s for assistance or to purchase a new license.'), $license_status_string, '<a target="_blank" href="https://snapcreek.com/">', '</a>');
}

$license_key = get_option(EZP_CSPE_Constants::LICENSE_KEY_OPTION_NAME, '');


?>

    <?php wp_nonce_field($nonce_action); ?>
    <input type="hidden" name="action" value="save" id="action">
    <input type="hidden" name="page"   value="<?php echo EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG ?>">
    <input type="hidden" name="tab"   value="licensing">

    <?php if ($action_updated === true) { ?>
        <div id="message" class="updated below-h2"><p><?php echo $action_response; ?></p></div>
    <?php } else if ($action_updated === false) { ?>	
        <div id="message" class="error below-h2"><p><?php echo $action_response; ?></p></div>
    <?php } ?>


    <!-- ===============================
    PLUG-IN SETTINGS -->
    <h3 class="title"><?php EZP_CSPE_U::_e("Plugin") ?> </h3>
    <hr size="1" />
    <table class="form-table">
        <tr valign="top">           
            <th scope="row"><label><?php EZP_CSPE_U::_e("License Key"); ?></label></th>
            <td>
                <input type="text" style="width:275px" <?php EZP_CSPE_U::echo_disabled($license_status == EZP_CSPE_License_Status::Valid); ?> name="_license_key" id="_license_key" value="<?php echo $license_key; ?>" />
                <p class="description">
                    <?php echo "<span style='$license_status_style'>$license_status_text</span>"; ?>
                </p>
            </td>
        </tr>	        
        <tr valign="top">           
            <th scope="row"><label><?php EZP_CSPE_U::_e("Activation"); ?></label></th>
            <td>
                <button onclick="EZP_CSPE.Licensing.ChangeActivationStatus(<?php echo (($license_status != EZP_CSPE_License_Status::Valid) ? 'true' : 'false'); ?>);return false;"><?php echo $activate_button_text; ?></button>
            </td>
        </tr>
    </table>   

<script type="text/javascript">
    jQuery(document).ready(function($) {
           
        EZP_CSPE= new Object();
        EZP_CSPE.Licensing = new Object();
        
        // which: 0=installer, 1=archive, 2=sql file, 3=log
        EZP_CSPE.Licensing.ChangeActivationStatus = function (activate) {    
            
            if(activate){
          
                $('#action').val('activate');
            } 
            else  {
                $('#action').val('deactivate');
            }

            $('#easy_pie_cs_main_form').submit();
        }
    });
</script>

