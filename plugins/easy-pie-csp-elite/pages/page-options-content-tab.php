<?php
//- Settings Logic -
$action_updated = null;
$global = EZP_CSPE_Global_Entity::get_instance();
$set_index = $global->active_set_index;
$set = EZP_CSPE_Set_Entity::get_by_id($set_index);
$content = EZP_CSPE_Content_Entity::get_by_id($set->content_index);
$config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
$error_string = "";

if (isset($_POST['action']) && $_POST['action'] == 'save') 
{
    check_admin_referer('easy-pie-csp-elite-save-content');

    // Artificially set the bools since they aren't part of the postback    
    // TODO
    $error_string = $content->set_post_variables($_POST);
    if ($error_string == "") {
        $action_updated = $content->save();
    }
}
?>

<style>
    .shorttext { width: 75px;}
</style>

<?php wp_nonce_field('easy-pie-csp-elite-save-content'); ?>
<input type="hidden" name="action" value="save"/>

<?php if ($error_string != "") :   ?>
    <div id="message" class="error below-h2"><p><?php echo EZP_CSPE_U::__('Errors present:') . "<br/> $error_string" ?></p></div>
<?php endif; ?>

<?php if ($action_updated) : ?>
    <div id="message" class="updated below-h2"><p><span><?php echo EZP_CSPE_U::__('Settings Saved.'); ?></span><strong style="margin-left:7px;"><?php echo '  ' . EZP_CSPE_U::__('If you have a caching plugin be sure to clear it.'); ?></strong></p></div>
<?php endif; ?>


<!-- ===================================
PAGE TITLE -->
<div class="postbox" style="margin-top:12px;" >
<div class="inside" >
<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Title") ?></h3>
<table class="form-table"> 
	<tr>
		<th scope="row">
			<?php echo EZP_CSPE_U::_e("Window Title") ?>
		</th>
		<td>
			<div class="compound-setting">                            
				<input class="long-input" name="title" type="text" value="<?php EZP_CSPE_U::_he($content->title); ?>" />
				<br/>
				<small><?php echo EZP_CSPE_U::_e("This is the browsers window title") ?></small>
			</div>
		</td>
	</tr>   
</table>
</div>
</div>
	
<!-- ===================================
MAIN CONTENT  -->
<div class="postbox" >
    <div class="inside" >
        <h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Content") ?></h3>		
		
		
		<!-- MAIN TEXT -->
		<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("MAIN TEXT") ?></div>		
        <table class="form-table"> 
            <tr>
                <th scope="row"><?php echo EZP_CSPE_U::_e("Headline") ?></th>
                <td><input class="long-input"  name="headline" type="text" value="<?php EZP_CSPE_U::_he($content->headline); ?>" /></td>
            </tr>   
            <tr>
                <th scope="row"><?php echo EZP_CSPE_U::_e("Description") ?></th>
                <td><textarea rows="5" cols="67" name="description"><?php EZP_CSPE_U::_he($content->description); ?></textarea></td>
            </tr>   
            <tr>
                <th scope="row"><?php echo EZP_CSPE_U::_e("Disclaimer") ?></th>
                <td><input class="long-input"  name="disclaimer" type="text" value="<?php EZP_CSPE_U::_he($content->disclaimer); ?>" /></td>
            </tr>               
            <tr>
                <th scope="row"><?php echo EZP_CSPE_U::_e("Footer") ?></th>
                <td><input class="long-input" name="footer" type="text" value="<?php EZP_CSPE_U::_he($content->footer); ?>" /></td>
            </tr>      
        </table> 
		
		
		<!-- EMAIL TEXT -->
		<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("EMAIL TEXT") ?></div>
        <table class="form-table"> 
            <tr>
                <th scope="row"><?php echo EZP_CSPE_U::_e("Email Placeholder") ?></th>
                <td><input class="long-input"  name="email_placeholder_text" type="text" value="<?php EZP_CSPE_U::_he($content->email_placeholder_text); ?>" /></td>
            </tr>   
            <tr>
                <th scope="row"><?php echo EZP_CSPE_U::_e("Name Placeholder") ?></th>
                <td><input class="long-input"  name="name_placeholder_text" type="text" value="<?php EZP_CSPE_U::_he($content->name_placeholder_text); ?>" /></td>
            </tr>
            <tr>
                <th scope="row"><?php echo EZP_CSPE_U::_e("Button") ?></th>
                <td><input name="email_button_text" type="text" value="<?php echo $content->email_button_text; ?>" /></td>
            </tr>       
        </table>
        <div style="margin-top:17px"><span class="description"><?php echo '*' . EZP_CSPE_U::__('Section relevant only if email collection is enabled in') . ' <a href="' . admin_url() . 'admin.php?page=' . EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '">' . self::__('settings') . '</a>'; ?></span></div>

</div>
</div>

<div class="postbox" >
<div class="inside" >
	<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Thank You Text") ?></h3>
	<table class="form-table"> 
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Headline") ?></th>
			<td><input class="long-input"  name="thank_you_headline" type="text" value="<?php EZP_CSPE_U::_he($content->thank_you_headline); ?>" /></td>
		</tr>
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Text") ?></th>
			<td><textarea rows="5" cols="67" name="thank_you_description"><?php EZP_CSPE_U::_he($content->thank_you_description); ?></textarea></td>
		</tr>
	</table>
	<div style="margin-top:17px"><span class="description"><?php echo '*' . EZP_CSPE_U::__('Section relevant only if email collection is enabled in') . ' <a href="' . admin_url() . 'admin.php?page=' . EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '">' . self::__('settings') . '</a>'; ?></span></div>
</div>
</div>


<div class="postbox" >
<div class="inside" >
	<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Countdown Text") ?></h3>
	<table class="form-table"> 
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Days") ?></th>
			<td> <input class="shorttext" name="countdown_days_text" type="text" value="<?php echo $content->countdown_days_text; ?>" /></td>
		</tr>       
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Hours") ?></th>
			<td><input class="shorttext" name="countdown_hours_text" type="text" value="<?php echo $content->countdown_hours_text; ?>" /></td>
		</tr>                   
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Minutes") ?></th>
			<td><input class="shorttext" name="countdown_min_text" type="text" value="<?php echo $content->countdown_min_text; ?>" /></td>
		</tr>       
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Seconds") ?></th>
			<td><input class="shorttext" name="countdown_sec_text" type="text" value="<?php echo $content->countdown_sec_text; ?>" /></td>
		</tr>       
	</table>
</div>
</div>