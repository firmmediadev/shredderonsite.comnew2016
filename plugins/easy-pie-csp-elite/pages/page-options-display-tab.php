<?php
$action_updated = null;

$global = EZP_CSPE_Global_Entity::get_instance();
$set_index = $global->active_set_index;
$set = EZP_CSPE_Set_Entity::get_by_id($set_index);
$display = EZP_CSPE_Display_Entity::get_by_id($set->display_index);
$content = EZP_CSPE_Content_Entity::get_by_id($set->content_index);
$config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
$error_string = "";

$font_dir_text = EZP_CSPE_U::__('Get Google font names from the ') . '<a href="https://www.google.com/fonts/" target="_blank">' . EZP_CSPE_U::__('Google Font Directory') . '</a>';
$google_font_help_text = '<a href="https://www.google.com/fonts/" target="_blank">' . EZP_CSPE_U::__('Font Names') . '</a>';

if (isset($_POST['action']) && $_POST['action'] == 'save')
{
	check_admin_referer('easy-pie-csp-elite-save-display');
	EZP_CSPE_U::log('past admin check');
	$temp_youtube_id = trim($_POST['youtube_video_id']);
	if ($temp_youtube_id != '')
	{
		parse_str(parse_url($temp_youtube_id, PHP_URL_QUERY), $var_array);
		if (array_key_exists('v', $var_array))
		{
			$_POST['youtube_video_id'] = $var_array['v'];
		}
	}

	// Artificially set the bools since they aren't part of the postback
	$display->background_tiling_enabled = false;
	$display->mute_background_video = false;
	$display->box_shadow_enable = false;
		
	$error_string = $display->set_post_variables($_POST);
	if ($error_string == "")
	{
		$action_updated = $display->save();
	}
	
	$content->logo_url = $_POST['logo_url'];
	
	$content->save();
}
?>
<style>    
    #easy-pie-cs-advanced h3 { cursor: default; margin-left:5px; margin-top:10px;}
    #easy-pie-cs-advanced table { margin-left:10px;}    
    #easy-pie-cs-quick-config-div { margin-top: 10px; width: 670px; overflow:hidden;}

    .template-image-holder { margin: 10px 10px 15px 10px; }
    .template-image { width:200px; opacity:0.4; border: black 1px solid;}
    .template-name { }
    .template-button { margin-top: 10px!important; }
    #easy-pie-cs-template-cancel-btn { }
    #easy-pie-cs-template-copy-btn { float:right;}    
    #template-dialog {background-color:white; box-shadow: 1px 7px 36px -5px rgba(34,34,34,1); border: #777 1px solid; padding:13px}
    #opacity-slider { margin-left:40px; width: 130px; }
    #opacity-slider .ui-slider-handle { width: 8px; text-align: center; }

    .ezp-cspe-radiodiv { margin-bottom:10px;}

    #easy-pie-cs-builtin-background-slider { width: 655px}
    #easy-pie-cs-builtin-background-slider img { height:70px; width:70px; margin-right:10px; margin-top:7px;}

	.ezp-google-font-entry td, .ezp-google-font-entry th { padding-top: 0; padding-bottom:0; }
	.ezp-google-font-entry p { margin-bottom:25px!important; }	
	.ezp-cspe-color-row { <?php EZP_CSPE_U::echo_display($display->background_type == EZP_CSPE_Display_Background_Type::Color, 'table-row', 'none'); ?> }
	.ezp-cspe-image-row { <?php EZP_CSPE_U::echo_display($display->background_type == EZP_CSPE_Display_Background_Type::Image, 'table-row', 'none'); ?> }
	.ezp-cspe-video-row { <?php EZP_CSPE_U::echo_display($display->background_type == EZP_CSPE_Display_Background_Type::Video, 'table-row', 'none'); ?> }
	
	#easy-pie-cs-background-image-url {width:550px}
</style>

<?php wp_nonce_field('easy-pie-csp-elite-save-display'); ?>
<input type="hidden" name="action" value="save"/>
<?php if ($error_string != "") : ?>
	<div id="message" class="error below-h2"><p><?php echo EZP_CSPE_U::__('Errors present:') . "<br/> $error_string" ?></p></div>
<?php endif; ?>

<?php if ($action_updated) : ?>
	<div id="message" class="updated below-h2">
		<p><span><?php echo EZP_CSPE_U::__('Settings Saved.'); ?></span><strong style="margin-left:7px;"><?php echo '  ' . EZP_CSPE_U::__('If you have a caching plugin be sure to clear it.'); ?></strong></p>
	</div>
<?php endif; ?>

<!-- ===================================
BACKGROUND -->
<div class="postbox"  style="margin-top:10px;">
<div class="inside">
<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Background") ?></h3>
<table class="form-table">
	<tr>
		<th scope="row"><?php echo EZP_CSPE_U::_e("Type") ?></th>
		<td>
			<div class="compound-setting">  
				<select id="ezp-cspe-background-type" name="background_type" onchange="easyPie.CSPE.Options.Display.ShowBackgroundSection();">
					<option <?php EZP_CSPE_U::echo_selected($display->background_type == EZP_CSPE_Display_Background_Type::Color) ?> value="<?php echo EZP_CSPE_Display_Background_Type::Color ?>"><?php EZP_CSPE_U::_e('Color'); ?></option>
					<option <?php EZP_CSPE_U::echo_selected($display->background_type == EZP_CSPE_Display_Background_Type::Image) ?> value="<?php echo EZP_CSPE_Display_Background_Type::Image ?>"><?php EZP_CSPE_U::_e('Image'); ?></option>
					<option <?php EZP_CSPE_U::echo_selected($display->background_type == EZP_CSPE_Display_Background_Type::Video) ?> value="<?php echo EZP_CSPE_Display_Background_Type::Video ?>"><?php EZP_CSPE_U::_e('Video'); ?></option>
				</select>
			</div>
		</td>
	</tr>
	<tr class="ezp-cspe-color-row">
		<th scope="row"><?php echo EZP_CSPE_U::_e("Color") ?></th>
		<td>
			<div class="compound-setting">  
				<input name="background_color" class="spectrum-picker" type="text" value="<?php echo $display->background_color; ?>"/>                   
			</div>
		</td>
	</tr>
	<tr class="ezp-cspe-image-row">
		<th scope="row"><?php echo EZP_CSPE_U::_e("Image") ?></th>                
		<td>   
			<div class="ezp-cspe-radiodiv">
				<div class="compound-setting">
					<div id="easy-pie-cs-builtin-background-slider"> 
						<?php
						$background_dir = EZP_CSPE_U::$PLUGIN_DIRECTORY . '/images/backgrounds/';
						$file_paths = glob($background_dir . '*.{jpg,png}', GLOB_BRACE);
						if ($file_paths != FALSE)
						{
							sort($file_paths);
							$image_index = 0;
							$build_in_background = false;
							foreach ($file_paths as $file_path)
							{
								$image_id = "built-in-bg-image-$image_index";
								$file_name = basename($file_path);
								$file_url = EZP_CSPE_U::$PLUGIN_URL . "/images/backgrounds/$file_name";
								if ($display->background_image_url != $file_url)
								{
									$opacity = 0.3;
								}
								else
								{
									$opacity = 1.0;
									$build_in_background = true;
								}
								echo "<img id='$image_id' src='$file_url' style='opacity:$opacity;cursor: pointer;'/>";
								$image_index++;
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="ezp-cspe-radiodiv">                                            
				<div>
					<span class="description"><?php echo EZP_CSPE_U::__('or use your own') . ':'; ?></span>
				</div>
				<div class="compound-setting">
					<input id="easy-pie-cs-background-image-url" name="background_image_url" value="<?php echo $display->background_image_url; ?>" />                            
					<button id="easy-pie-cs-background-image-button"><?php EZP_CSPE_U::_e("Upload"); ?></button>
					<img id="easy-pie-cs-background-image-preview" style="display: <?php echo ($build_in_background || $display->background_image_url == '') ? 'none' : 'block' ?> ;width:80px;height:80px;margin-top:8px;" src="<?php echo $display->background_image_url; ?>" />
				</div>                                                   
			</div>

		</td>
	</tr>            
	<tr class="ezp-cspe-image-row">
		<th scope="row">
			<?php echo EZP_CSPE_U::_e("Tile") ?>
		</th>
		<td>
			<div class="compound-setting">                                            
				<input type="checkbox" name="background_tiling_enabled" value="true" <?php echo $display->background_tiling_enabled == 'true' ? 'checked' : ''; ?> />
				<span><?php EZP_CSPE_U::_e("Enabled") ?></span>                        
			</div>                        
			<div>
				<span class="description"><?php echo EZP_CSPE_U::__('Image covers screen when tiling is disabled') . '.'; ?></span>
			</div>
		</td>
	</tr>
	<tr class="ezp-cspe-video-row">
		<th scope="row">
			<?php echo EZP_CSPE_U::_e("YouTube ID") ?>
		</th>
		<td>
			<div class="compound-setting">                            
				<input name="youtube_video_id" type="text" value="<?php echo $display->youtube_video_id; ?>" />  <i class="fa fa-youtube fa-2x" aria-hidden="true"></i> 
				<div>
					<span class="description"><?php echo sprintf('YouTube <a href="%s" target="blank">video ID.</a>', 'http://docs.joeworkman.net/rapidweaver/stacks/youtube/video-id'); ?> Note: Does not show on mobile. </span>
				</div>
			</div>
		</td>
	</tr>   
	<tr class="ezp-cspe-video-row">
		<th scope="row">
			<?php echo EZP_CSPE_U::_e("Sound") ?>
		</th>
		<td>
			<div class="compound-setting">                                            
				<input type="checkbox" name="mute_background_video" value="true" <?php echo $display->mute_background_video == 'true' ? 'checked' : ''; ?> />
				<span><?php EZP_CSPE_U::_e("Mute") ?></span>                        
			</div>                        
		</td>
	</tr>  
</table>

</div>
</div>


<!-- ===================================
CONTENT AREA  -->
<div class="postbox" >
<div class="inside" >
<h3 class="ezp-cspe-subtitle"><?php EZP_CSPE_U::_e("Content") ?></h3>		
		
	<!-- CONTENT BOX -->
	<div class="ezp-cspe-subtitle2" style="margin-top:15px"><?php EZP_CSPE_U::_e("CONTENT BOX") ?></div>	
	
	<table class="form-table">
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Box Shadow") ?></th>
			<td>
				<div class="compound-setting">     
					<input name="box_shadow_enable" value="true" type="checkbox" <?php EZP_CSPE_U::echo_checked($display->box_shadow_enable); ?>/>                   
				</div>
			</td>
		</tr>  
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Opacity") ?></th>
			<td>
				<div class="compound-setting">                                                    
					<div style="display:none;"><input class='narrow-input' id="content_box_opacity" name="content_box_opacity" type="text" value="<?php echo $display->content_box_opacity ?>" readonly="true"/>                        </div>                        
					<div id="opacity-display-value" style="float:left;">hi</div>
					<div style="padding-top:2px;"><div id="opacity-slider"></div></div>                        
				</div>

			</td>
		</tr>    
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Color") ?></th>
			<td>
				<div class="compound-setting">     
					<input name="content_box_color" class="spectrum-picker" type="text" value="<?php echo $display->content_box_color ?>"/>                   
				</div>
			</td>
		</tr>  
	</table>
		
	
	<!-- LOGO -->
	<div class="ezp-cspe-subtitle2"><?php EZP_CSPE_U::_e("LOGO") ?></div>
	
	<table>
		<tr>
			<td>
				<table class="form-table"> 
					<tr>
						<th scope="row">
							<?php echo EZP_CSPE_U::_e("Image") ?>
						</th>
						<td>

								<input id="easy-pie-cs-logo-url" name="logo_url" value="<?php echo $content->logo_url; ?>" />                            
								<button id="easy-pie-cs-logo-button"><?php EZP_CSPE_U::_e("Upload"); ?></button>                                              
						</td>
					</tr>
					<tr>
						<th scope="row"><?php echo EZP_CSPE_U::_e("Width") ?></th>
						<td>
							<div class="compound-setting">                            
								<input class='narrow-input' name="logo_width" type="text" value="<?php echo $display->logo_width; ?>" />
								<span class="description"><?php echo '*' . EZP_CSPE_U::__('Append px or %'); ?></span>
							</div>
						</td>
					</tr>   
					<tr>
						<th scope="row"><?php echo EZP_CSPE_U::_e("Height") ?></th>
						<td>
							<div class="compound-setting">                            
								<input class='narrow-input' name="logo_height" type="text" value="<?php echo $display->logo_height; ?>" />
								<span class="description"><?php echo '*' . EZP_CSPE_U::__('Append px or %'); ?></span>
							</div>
						</td>
					</tr>   
				</table>
			</td>
			<td style="vertical-align: top; padding: 10px 0 0 15px;">
				<i><?php echo  EZP_CSPE_U::__('Thumbnail Preview'); ?></i>
					<img id="easy-pie-cs-logo-preview" style="display: <?php echo $content->logo_url == '' ? 'none' : 'block' ?> ;width:80px;height:80px;margin-top:8px;" src="<?php echo $content->logo_url; ?>" />  
			</td>
		</tr>
		
	</table>
	

		
		
	<!-- TEXT -->
	<div class="ezp-cspe-subtitle2"><?php EZP_CSPE_U::_e("TEXT") ?></div>
	<table class="form-table">
		<tr>        
			<?php EZP_CSPE_Display_Entity::display_font_field_row('Headline', 'text_headline', $display, 'ezp-cspe-headline-google-font') ?>
		</tr>
		<tr id="ezp-cspe-headline-google-font" class="ezp-google-font-entry">		
			<?php $display->display_google_font_field_row('text_headline'); ?>
		</tr>
		<tr>
			<?php EZP_CSPE_Display_Entity::display_font_field_row('Description', 'text_description', $display, 'ezp-cspe-description-google-font') ?>
		</tr>
		<tr id="ezp-cspe-description-google-font" class="ezp-google-font-entry">		
			<?php $display->display_google_font_field_row('text_description'); ?>
		</tr>
		<tr>
			<?php EZP_CSPE_Display_Entity::display_font_field_row('Disclaimer', 'text_disclaimer', $display, 'ezp-cspe-disclaimer-google-font') ?>
		</tr>
		<tr id="ezp-cspe-disclaimer-google-font" class="ezp-google-font-entry">		
			<?php $display->display_google_font_field_row('text_disclaimer'); ?>
		</tr>
		<tr>
			<?php EZP_CSPE_Display_Entity::display_font_field_row('Footer', 'text_footer', $display, 'ezp-cspe-footer-google-font') ?>
		</tr>  
		<tr id="ezp-cspe-footer-google-font" class="ezp-google-font-entry">		
			<?php $display->display_google_font_field_row('text_footer'); ?>
		</tr>
		<tr>        
			<?php EZP_CSPE_Display_Entity::display_font_field_row('Thank You Headline', 'text_thankyou_headline', $display, 'ezp-cspe-thankyou-headline-google-font') ?>
		</tr>
		<tr id="ezp-cspe-thankyou-headline-google-font" class="ezp-google-font-entry">		
			<?php $display->display_google_font_field_row('text_thankyou_headline'); ?>
		</tr>
		<td colspan="2" style="padding-left: 230px"><span class="description"><?php echo '*' . EZP_CSPE_U::__('For Size Append px or em'); ?></span></td>
	</table>
	
	
	<!-- EMAIL BUTTON -->
	<div class="ezp-cspe-subtitle2"><?php EZP_CSPE_U::_e("EMAIL BUTTON") ?></div>
	<table class="form-table">
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Color") ?></th>
			<td>
				<div class="compound-setting">                            
					<input name="email_button_color" class="spectrum-picker" type="text" value="<?php echo $display->email_button_color; ?>"/>
				</div>
			</td>
		</tr>  	
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Width") ?></th>
			<td>
				<div class="compound-setting">                            
					<input class='narrow-input' name="email_button_width" type="text" value="<?php echo $display->email_button_width; ?>" />
					<span class="description"><?php echo '*' . EZP_CSPE_U::__('Append px or %'); ?></span>
				</div>
			</td>
		</tr>   
		<tr>
			<th scope="row"><?php echo EZP_CSPE_U::_e("Height") ?></th>
			<td>
				<div class="compound-setting">                            
					<input class='narrow-input' name="email_button_height" type="text" value="<?php echo $display->email_button_height; ?>" />
					<span class="description"><?php echo '*' . EZP_CSPE_U::__('Append px or %'); ?></span>
				</div>
			</td>
		</tr> 
		<tr>
			<?php EZP_CSPE_Display_Entity::display_font_field_row("Font", 'email_button', $display, 'ezp-cspe-email-button-google-font'); ?>
		</tr>
		<tr id="ezp-cspe-email-button-google-font" class="ezp-google-font-entry">		
			<?php $display->display_google_font_field_row('email_button'); ?>
		</tr>
	</table> 	
	
</div>
</div>




<!-- ===================================
ADVANCED  -->
<div class="postbox" >
<div class="inside" >
	<h3 class="ezp-cspe-subtitle"><span style="font-weight:bold"><?php EZP_CSPE_U::_e('Advanced'); ?><span></h3>
	<table class="form-table">
		<tr valign="top">
			<th scope="row"><?php EZP_CSPE_U::_e("Custom CSS") ?></th><td>
				<div>
					<textarea cols="75" rows="12" id="easy-pie-cs-field-junk" name="css" style="font-size:12px"><?php echo $display->css; ?></textarea>
				</div>             
			</td>
		</tr>
	</table>
	<div style="margin-top:4px;"><a target="_blank" href="https://snapcreek.com/ezp-coming-soon/docs/faqs-tech/">
		<span class="description"><?php EZP_CSPE_U::_e('Page Styling Tips'); ?></span></a>
	</div>		
</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function ($) 
	{
		easyPie.CSPE = {};
		easyPie.CSPE.Options = {};
		easyPie.CSPE.Options.Display = {};

		easyPie.CSPE.Options.Display.ShowBackgroundSection = function () {

			var backgroundType = $("#ezp-cspe-background-type").val();
			
			if (backgroundType == <?php echo EZP_CSPE_Display_Background_Type::Color ?>) {

				$('.ezp-cspe-color-row').show();
				$('.ezp-cspe-image-row').hide();
				$('.ezp-cspe-video-row').hide();
			} else if (backgroundType == <?php echo EZP_CSPE_Display_Background_Type::Image ?>) {

				$('.ezp-cspe-color-row').hide();
				$('.ezp-cspe-image-row').show();
				$('.ezp-cspe-video-row').hide();
			} else {

				$('.ezp-cspe-color-row').hide();
				$('.ezp-cspe-image-row').hide();
				$('.ezp-cspe-video-row').show();
			}
		}
	});
</script>
