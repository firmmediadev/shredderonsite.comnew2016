<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once(dirname(__FILE__) . '/Utilities/class-ezp-cspe-utility.php');

require_once(EZP_CSPE_U::$PLUGIN_DIRECTORY . '/../../../wp-admin/includes/upgrade.php');

//require_once("class-easy-pie-options.php");
require_once("Entities/class-ezp-cspe-global-entity.php");

require_once('class-ezp-cspe-plugin-base.php');
require_once('class-ezp-cspe-constants.php');

require_once(dirname(__FILE__) . '/Utilities/class-ezp-cspe-email-utility.php');
require_once(dirname(__FILE__) . '/Utilities/class-ezp-cspe-render-utility.php');
require_once(dirname(__FILE__) . '/Utilities/class-ezp-cspe-test-utility.php');
require_once(dirname(__FILE__) . '/Utilities/class-ezp-cspe-license-utility.php');

require_once(dirname(__FILE__) . '/Entities/class-ezp-cspe-access-key-entity.php');

if (!class_exists('EZP_CSPE')) {

    /**
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE extends EZP_CSPE_Plugin_Base {

        /**
         * Constructor
         */
        function __construct($plugin_file_path) {
            
            parent::__construct(EZP_CSPE_Constants::PLUGIN_SLUG);

            $this->add_class_action('plugins_loaded', 'plugins_loaded_handler');
			$this->add_class_action('admin_enqueue_scripts', 'admin_enqueue_scripts_handler');

            $entity_table_present = EZP_CSPE_Query_Utility::is_table_present(EZP_CSPE_JSON_Entity_Base::DEFAULT_TABLE_NAME);

            if ($entity_table_present) 
            {
                $global = EZP_CSPE_Global_Entity::get_instance();

                $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);

                // Determine if we need to turn things off based on countdown settings
                if($config->coming_soon_mode_on)
                {
                    if($config->disable_after_countdown && (trim($config->countdown_due_date) != ''))
                    {
                        $countdown_timestamp = strtotime($config->countdown_due_date);
                        
                        $reverse_gmt_timestamp = $countdown_timestamp - (3600 * get_option('gmt_offset'));                               
                        
                        $current_time = time();
                        
                        if($current_time > $reverse_gmt_timestamp)
                        {
                            EZP_CSPE_U::log("Turning off coming soon since it is now and we are waiting for " . $current_time . ' ' . $reverse_gmt_timestamp);
                            
                            $config->coming_soon_mode_on = false;
                            $config->save();
                        }
                        else
                        {
                            EZP_CSPE_U::log("Not turning off coming soon since it is now and we are waiting for " . $current_time . ' ' . $reverse_gmt_timestamp);
                        }
                    }
                }
                
                $coming_soon_mode_on = $config->coming_soon_mode_on;
                                               
                $in_preview = isset($_REQUEST['EZP_CSPE_preview']) && ($_REQUEST['EZP_CSPE_preview'] == 'true');
                
                $access_key_validated = false;
                
                if(isset($_COOKIE[EZP_CSPE_Constants::AK_COOKIE_NAME]))
                {
                    $cookie_access_key_string = $_COOKIE[EZP_CSPE_Constants::AK_COOKIE_NAME];
                    
                    $access_key = EZP_CSPE_Access_Key_Entity::get_by_key($cookie_access_key_string);
                    
                    if($access_key != null)
                    {
                        EZP_CSPE_U::log("Access granted given cookie key $cookie_access_key_string");
                        $access_key_validated = true;
                    }
                    else
                    {
                        // Remove cookie since it's no longer valid
                        EZP_CSPE_U::log("Stale access key from cookie so removing $cookie_access_key_string");
                        
                        setcookie(EZP_CSPE_Constants::AK_COOKIE_NAME, '', time() - 3600);
                    }
                }
                else
                {
                    //EZP_CSPE_U::log("Cookie " . EZP_CSPE_Constants::AK_COOKIE_NAME . ' not present');
                }
                
                if($access_key_validated === false)
                {
                    if(isset($_REQUEST[EZP_CSPE_Constants::AK_QUERY_KEY]))
                    {
                        $request_access_key_string = $_REQUEST[EZP_CSPE_Constants::AK_QUERY_KEY];

                        $access_key = EZP_CSPE_Access_Key_Entity::get_by_key($request_access_key_string);

                        if($access_key == null)
                        {
                            EZP_CSPE_U::log("Attempt to access the site with bad access key $request_access_key_string");
                        }
                        else
                        {
                            
                            $cookie_set = setcookie(EZP_CSPE_Constants::AK_COOKIE_NAME, $request_access_key_string, time() + 60 * 60 * 24 * 7, '/');            // For now hard coding a week
                            
                            EZP_CSPE_U::log("Accessing the site with access key $request_access_key_string and setting cookie " . EZP_CSPE_Constants::AK_COOKIE_NAME . ':' . EZP_CSPE_U::bool_to_string($cookie_set));
                            $access_key_validated = true;
                        }
                    }
                }
            } else {

                // On activation so we don't have the tables yet
                $coming_soon_mode_on = false;
                $in_preview = false;
            }

            //EZP_CSPE_U::log("coming soon mode on $coming_soon_mode_on");
            
            $coming_soon_mode_on = ($coming_soon_mode_on && ($access_key_validated == false)); 
                    
         //   EZP_CSPE_U::log("coming soon mode on $coming_soon_mode_on access key validated $access_key_validated");
            // RSR TODO - is_admin() just says if admin panel is attempting to be displayed - NOT to see if someone is an admin
            if (is_admin() && !$in_preview) {

                //EZP_CSPE_Utility::debug("admin true");

				// Account for the possibility the state was just changed
				if (isset($_GET['page']) && (strpos($_GET['page'], EZP_CSPE_Constants::PLUGIN_SLUG) === 0) && isset($_POST['coming_soon_mode_on']))
				{
					$coming_soon_mode_on = (bool)$_POST['coming_soon_mode_on'];
				}
					
                if ($coming_soon_mode_on) {

					$this->add_class_action('admin_bar_menu', 'display_coming_soon_admin_bar_alert');
                }
                
                $this->add_class_action("admin_notices", "display_license_notice");

                //- Hook Handlers
                register_activation_hook($plugin_file_path, array('EZP_CSPE', 'activate'));
                register_deactivation_hook($plugin_file_path, array('EZP_CSPE', 'deactivate'));
                register_uninstall_hook($plugin_file_path, array('EZP_CSPE', 'uninstall'));

                //- Actions
                $this->add_class_action('admin_init', 'admin_init_handler');
                $this->add_class_action('admin_menu', 'add_to_admin_menu');

                $this->add_class_action('wp_ajax_EZP_CSPE_export_all_subscribers', 'ws_export_all_subscribers');

                $this->add_class_action('wp_ajax_EZP_CSPE_purge_contact', 'ws_purge_contact');

                $this->add_class_action('wp_ajax_EZP_CSPE_test', 'ws_test');

                $this->add_class_action('wp_ajax_EZP_CSPE_copy_template', 'ws_copy_template');
                
            } else {

                //EZP_CSPE_Utility::debug("admin false");
                if ($coming_soon_mode_on || $in_preview) {
                //    EZP_CSPE_U::log("displaying coming soon page");
                    $this->add_class_action('template_redirect', 'display_coming_soon_page');
                }
            }
        }

        function ws_export_all_subscribers() {

            if (isset($_REQUEST['_wpnonce'])) {

                $_wpnonce = $_REQUEST['_wpnonce'];

                if (wp_verify_nonce($_wpnonce, 'easy-pie-cspe-export-subscribers')) {

                    header("Pragma: public");
                    header("Expires: 0");
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Cache-Control: private", false);
                    header("Content-Type: application/octet-stream");
                    header("Content-Disposition: attachment; filename=\"subscribers.csv\";");
                    header("Content-Transfer-Encoding: binary");

                    //$subscribers = EZP_CSPE_Query_Utility::get_subscriber_list(-1);
                    $subscribers = EZP_CSPE_Subscriber_Entity::get_all();

                    echo "First Name,Last Name,Email Address\r\n";
                    foreach ($subscribers as $subscriber) 
                    {                         
                        $names = EZP_CSPE_Email_Utility::split_names($subscriber->friendly_name);
                    
                        if($names != null)
                        {
                            $first_name = $names[0];
                            $last_name = $names[1];

                            /* @var $subscriber EZP_CSPE_Subscriber_Entity */
    //                        if ($subscriber->creation_date != '') {
    //                            
    //                            //   $localized_date = date_i18n(get_option('date_format'), strtotime($subscriber->subscription_date));
    //                            //$date_text = date('n/j/Y', strtotime($subscriber->));
    //                            $date_text = $subscriber->get_local_creation_date();
    //                        } else {
    //                            //   $localized_date = '';
    //                            $date_text = 'bob';
    //                        }

                            echo "$first_name,$last_name,$subscriber->email_address\r\n";
                        }
                    }

                    exit;
                } else {

                    EZP_CSPE_U::log("ws_export_all_subscribers: Security violation. Nonce doesn't properly match!");
                }
            } else {

                EZP_CSPE_U::log("ws_export_all_subscribers: Security violation. Nonce doesn't exist!");
            }
        }

        function ws_purge_contact() {
            $request = stripslashes_deep($_REQUEST);

            if (isset($request['_wpnonce'])) {

                $_wpnonce = $request['_wpnonce'];

                if (wp_verify_nonce($_wpnonce, 'easy-pie-cs-change-subscribers')) {

                    if (isset($request['contact_id'])) {

                        $contact_id = $request['contact_id'];

                        EZP_CSPE_Subscriber_Entity::delete_by_id($contact_id);
                    } else {
                        EZP_CSPE_U::log("ws_purge_contact: contact id not set");
                    }
                } else {

                    EZP_CSPE_U::log("ws_purge_contact: Security violation. Nonce doesn't properly match!");
                }
            } else {

                EZP_CSPE_U::log("ws_purge_contact: Security violation. Nonce doesn't exist!");
            }
        }

        function ws_test() {

            $post = stripslashes_deep($_POST);

            if (isset($post['type'])) {

                $type = $post['type'];

                switch ($type) {
                    case 'add_subscribers':
                        EZP_CSPE_Test_Utility::add_test_subscribers($post);
                        break;

                    default:
                        EZP_CSPE_U::log('ws_test: Unknown test type');
                }
            }
        }
		
        function ws_copy_template() {

            $post = stripslashes_deep($_POST);

            if (isset($post['template_key'])) {

                $template_key = $post['template_key'];

                $global = EZP_CSPE_Global_Entity::get_instance();

                $set_index = $global->active_set_index;

                $set = EZP_CSPE_Set_Entity::get_by_id($set_index);

                $display = EZP_CSPE_Display_Entity::get_by_id($set->display_index);

                // RSR TODO: Have to be careful here - ensure that a single error won't hose up the state of the system - TODO: Maybe a reset to defaults option to clean out db?
                $display->delete();

                $new_display = EZP_CSPE_Display_Entity::create_from_template($template_key);

                $new_display->save();

                $set->display_index = $new_display->id;

                $set->save();
            }
        }

        function add_class_action($tag, $method_name) {

            return add_action($tag, array($this, $method_name));
        }

        function add_class_filter($tag, $method_name) {

            return add_filter($tag, array($this, $method_name));
        }
        
		public function display_coming_soon_admin_bar_alert($wp_admin_bar)
		{
			$wp_admin_bar->add_node( array(
				'id'     => 'easy-pie-csp-elite-on',
				'href' => admin_url() . "admin.php?page=" . EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG,
				'parent' => 'top-secondary',
				'title'  => EZP_CSPE_U::__('Coming Soon On'),
				'meta'   => array( 'class' => 'easy-pie-csp-elite-on' ),
			) );
		}      
        
        public function display_license_notice() 
        {
            $on_licensing_tab = (isset($_REQUEST['tab']) && ($_REQUEST['tab'] === 'licensing'));

//            if(($on_licensing_tab === false) && (EZP_CSPE_License_Utility::get_license_status(false) !== EZP_CSPE_License_Status::Valid))
//            {
//                $activate_url = admin_url() . "admin.php?page=" . EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=licensing';
//               
//                $mailto_url = 'mailto:support@snapcreek.com';
//
//                echo sprintf('<div class="error"><p><b>Warning!</b> Your Coming Soon Page: Elite license is invalid or disabled so you won\'t get important updates! <a href="%1$s">Activate your license</a> or <a target="_blank" href="%2$s">contact us to get one</a>.</p></div>', $activate_url, $mailto_url);
//            }
			
			if($on_licensing_tab === false)
			{
				$license_page_url = admin_url() . "admin.php?page=" . EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG . '&tab=licensing';
				
				$license_status = EZP_CSPE_License_Utility::get_license_status(false);
								
				if($license_status === EZP_CSPE_License_Status::Expired)
				{				
					$license_key = get_option(EZP_CSPE_Constants::LICENSE_KEY_OPTION_NAME, '');

					$renewal_url = 'https://snapcreek.com/checkout?edd_license_key=' . $license_key;

					echo sprintf('<div class="error"><p><b>Warning!</b> Your Coming Soon Elite license has expired so you aren\'t getting important updates! <a target="_blank" href="%1$s">Renew license now</a>. After renewing be sure to <a href="%2$s">refresh license status</a>.</div>', $renewal_url, $license_page_url);
				}
				else if($license_status !== EZP_CSPE_License_Status::Valid)
				{
					$mailto_url = 'mailto:support@snapcreek.com';

					echo sprintf('<div class="error"><p><b>Warning!</b> Your Coming Soon Elite license is invalid or disabled so you won\'t get important updates! <a href="%1$s">Activate your license</a> or <a target="_blank" href="%2$s">contact us to get one</a>.</p></div>', $license_page_url, $mailto_url);
				}
			}
        }

        /**
         * Display the maintenance page
         */
        public function display_coming_soon_page() {
            $global = EZP_CSPE_Global_Entity::get_instance();

            $set_index = $global->active_set_index;

            $set = EZP_CSPE_Set_Entity::get_by_id($set_index);

            $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);

            $in_preview = isset($_REQUEST['EZP_CSPE_preview']) && ($_REQUEST['EZP_CSPE_preview'] == 'true');

			$ip_allowed = false;
			if(trim($config->allowed_ips) != "") {
                
                $ip_allowed = EZP_CSPE_U::is_current_ip_allowed($config);
            } else {
                
                $ip_allowed = false;
            }
			
			if($ip_allowed == false)
			{
				if(trim($config->unfiltered_urls) != "") {

					$is_unfiltered = EZP_CSPE_U::is_current_url_unfiltered($config);
				} else {

					$is_unfiltered = false;
				}
			}
						
            if (!$ip_allowed && !$is_unfiltered && (!is_user_logged_in() || $in_preview)) {

                if ($config->return_code == 503) {

                    header('HTTP/1.1 503 Service Temporarily Unavailable');
                    header('Status: 503 Service Temporarily Unavailable');
                    header('Retry-After: 86400'); // RSR TODO: Put in the retry time later
                } else {

                    header('HTTP/1.1 200 OK');
                }

                $__dir__ = dirname(__FILE__);

                $page = $__dir__ . "/../mini-themes/base-responsive/index.php";

                $page_url = content_url('plugins/easy-pie-csp-elite/mini-themes/base-responsive');

                require($page);


                exit();
            }
        }

        // <editor-fold defaultstate="collapsed" desc="Hook Handlers">
        public static function activate() {

            EZP_CSPE_U::log("activate");

            // All version stuff is not in upgrade processing
            
//            $installed_ver = get_option(EZP_CSPE_Constants::PLUGIN_VERSION_OPTION_KEY);

            //rsr todo       if($installed_ver != EZP_CSPE_Constants::PLUGIN_VERSION)
            {
                EZP_CSPE_JSON_Entity_Base::init_table();
                
                EZP_CSPE_Subscriber_Entity::init_table();
                EZP_CSPE_Global_Entity::initialize_plugin_data();

//                update_option(EZP_CSPE_Constants::PLUGIN_VERSION_OPTION_KEY, EZP_CSPE_Constants::PLUGIN_VERSION);
            }
        }

        public static function deactivate() {

            EZP_CSPE_U::log("deactivate");
        }

        public static function uninstall() {

            EZP_CSPE_U::log("uninstall");
        }

        // </editor-fold>

        public function enqueue_scripts() {

            $jsRoot = plugins_url() . "/" . EZP_CSPE_Constants::PLUGIN_SLUG . "/js";

            wp_enqueue_script('jquery');
            wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-effects-core');		

            $jQueryPluginRoot = plugins_url() . "/" . EZP_CSPE_Constants::PLUGIN_SLUG . "/jquery-plugins";

            if (isset($_GET['page'])) {


                if ($_GET['page'] == EZP_CSPE_Constants::$TEMPLATE_SUBMENU_SLUG) {

                    if (!isset($_GET['tab']) || ($_GET['tab'] == 'display')) {


                        wp_enqueue_script('jquery-ui-slider');
                        wp_enqueue_script('spectrum.min.js', $jQueryPluginRoot . '/spectrum-picker/spectrum.min.js', array('jquery'), EZP_CSPE_Constants::PLUGIN_VERSION);
                    } else {
                        // Implies it is the content tab
//
//                        wp_enqueue_script('jquery-ui-datepicker');
//                        wp_enqueue_script('jquery-ui-slider');
//                        wp_enqueue_script('jquery-ui-timepicker-addon.js', $jQueryPluginRoot . '/jquery-timepicker/jquery-ui-timepicker-addon.js', array('jquery-ui-datepicker', 'jquery-ui-slider'), EZP_CSPE_Constants::PLUGIN_VERSION);
                    }

                    wp_enqueue_media();
                } else if (($_GET['page'] == EZP_CSPE_Constants::$SUBSCRIBERS_SUBMENU_SLUG)) {


                    wp_enqueue_script('jquery-ui-dialog');
                } else if (($_GET['page'] == EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG)) {

                    wp_enqueue_script('jquery-ui-dialog');

                    wp_enqueue_script('jquery-ui-datepicker');
                    wp_enqueue_script('jquery-ui-slider');
                    wp_enqueue_script('jquery-ui-timepicker-addon.js', $jQueryPluginRoot . '/jquery-timepicker/jquery-ui-timepicker-addon.js', array('jquery-ui-datepicker', 'jquery-ui-slider'), EZP_CSPE_Constants::PLUGIN_VERSION);
                }
            }
        }

        /**
         *  enqueue_styles
         *  Loads the required css links only for this plugin  */
        public function enqueue_styles() {
            $styleRoot = plugins_url() . "/" . EZP_CSPE_Constants::PLUGIN_SLUG . "/styles";

            wp_register_style('jquery-ui-min-css', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css', array(), EZP_CSPE_Constants::PLUGIN_VERSION);
            wp_enqueue_style('jquery-ui-min-css');



            wp_register_style('easy-pie-cspe-styles.css', $styleRoot . '/easy-pie-cspe-styles.css', array(), EZP_CSPE_Constants::PLUGIN_VERSION);
            wp_enqueue_style('easy-pie-cspe-styles.css');
			
			//FontAwesome
			$font_awesome_directory = plugins_url() . "/" . EZP_CSPE_Constants::PLUGIN_SLUG . "/lib/font-awesome/css/";
            wp_register_style('ezpcse-fontawesome', $font_awesome_directory . 'font-awesome.min.css', array(), '4.6.3');
            wp_enqueue_style('ezpcse-fontawesome');
                        

            $jQueryPluginRoot = plugins_url() . "/" . EZP_CSPE_Constants::PLUGIN_SLUG . "/jquery-plugins";
            //       wp_enqueue_style('jquery.eyecon.colorpicker.colorpicker', $jQueryPluginRoot . '/colorpicker/css/colorpicker.css', array(), EZP_CSPE_Constants::PLUGIN_VERSION);

            if (isset($_GET['page']) )
            {
                if($_GET['page'] == EZP_CSPE_Constants::$TEMPLATE_SUBMENU_SLUG)
                {
                    if (!isset($_GET['tab']) || ($_GET['tab'] == 'display')) 
                    {
                        wp_enqueue_style('spectrum.css', $jQueryPluginRoot . '/spectrum-picker/spectrum.css', array(), EZP_CSPE_Constants::PLUGIN_VERSION);                    
                    }                     
                }                
                else if($_GET['page'] == EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG)
                {
                    wp_enqueue_style('jquery-ui-timepicker-addon.css', $jQueryPluginRoot . '/jquery-timepicker/jquery-ui-timepicker-addon.css', array(), EZP_CSPE_Constants::PLUGIN_VERSION);
                }
            }
        }

        // <editor-fold defaultstate="collapsed" desc=" Action Handlers ">
        public function plugins_loaded_handler() {

            $this->init_localization();
            $this->upgrade_processing();
        }
		
		public function admin_enqueue_scripts_handler() {
			
			$styleRoot = plugins_url() . "/" . EZP_CSPE_Constants::PLUGIN_SLUG . "/styles";
			
			wp_register_style('easy-pie-cspe-common-admin-styles.css', $styleRoot . '/easy-pie-cspe-common-admin-styles.css', array(), EZP_CSPE_Constants::PLUGIN_VERSION);
            wp_enqueue_style('easy-pie-cspe-common-admin-styles.css');
		}

        public function init_localization() {

            load_plugin_textdomain(EZP_CSPE_Constants::PLUGIN_SLUG, false, EZP_CSPE_Constants::PLUGIN_SLUG . '/languages/');
        }

        public function admin_init_handler() {

            $this->add_filters_and_actions();
        }

        private function add_filters_and_actions() {

            add_filter('plugin_action_links', array($this, 'get_action_links'), 10, 2);
        }    
        
        function get_action_links($links, $file) {

            if ($file == "easy-pie-csp-elite/easy-pie-csp-elite.php") {

                $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=' . EZP_CSPE_Constants::PLUGIN_SLUG . '">Settings</a>';

                array_unshift($links, $settings_link);
            }

            return $links;
        }

        function upgrade_processing() 
        {                                   
            $installed_ver = get_option(EZP_CSPE_Constants::PLUGIN_VERSION_OPTION_KEY);
            
			$global = EZP_CSPE_Global_Entity::get_instance();
			$config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);                    
			$set_index = $global->active_set_index;
			$set = EZP_CSPE_Set_Entity::get_by_id($set_index);
			$content = EZP_CSPE_Content_Entity::get_by_id($set->content_index);
			$display = EZP_CSPE_Display_Entity::get_by_id($set->display_index);
				
            if(version_compare($installed_ver, '1.1.0', '<'))
            {                                      
                if($config->countdown_due_date == '')
                {
                    $config->countdown_due_date = $content->countdown_due_date;
                    $config->save();
                }
                
                $content->countdown_due_date = '';  // Now deprecated 
                                
                $content->save();
            }    
            
            /* Standard Version Update */
            if(version_compare($installed_ver, EZP_CSPE_Constants::PLUGIN_VERSION, 'ne'))
            {                
                update_option(EZP_CSPE_Constants::PLUGIN_VERSION_OPTION_KEY, EZP_CSPE_Constants::PLUGIN_VERSION);
                
                EZP_CSPE_U::log("Updated version from $installed_ver to " . EZP_CSPE_Constants::PLUGIN_VERSION);
            }
			
			if($display->background_type == EZP_CSPE_Display_Background_Type::NotSet)
			{
				$display->set_background_type();
				$display->save();
			}
        }

        // </editor-fold>
        
        public function add_to_admin_menu() {

            $perms = 'manage_options';

            add_menu_page('Coming Soon & Maintenance Elite', 'Coming Soon', $perms, EZP_CSPE_Constants::PLUGIN_SLUG, array($this, 'display_template_options_page'), EZP_CSPE_U::$PLUGIN_URL . '/images/easy-pie-cspe-menu-icon.png');
            $template_page_hook_suffix = add_submenu_page(EZP_CSPE_Constants::PLUGIN_SLUG, $this->__('Coming Soon & Maintenance Elite Template'), $this->__('Template'), $perms, EZP_CSPE_Constants::$TEMPLATE_SUBMENU_SLUG, array($this, 'display_template_options_page'));			            
			$settings_page_hook_suffix = add_submenu_page(EZP_CSPE_Constants::PLUGIN_SLUG, $this->__('Coming Soon & Maintenance Elite Settings'), $this->__('Settings'), $perms, EZP_CSPE_Constants::$SETTINGS_SUBMENU_SLUG, array($this, 'display_settings_page'));
            $tools_page_hook_suffix = add_submenu_page(EZP_CSPE_Constants::PLUGIN_SLUG, $this->__('Coming Soon & Maintenance Elite Tools'), $this->__('Tools'), $perms, EZP_CSPE_Constants::$TOOLS_SUBMENU_SLUG, array($this, 'display_tools_page'));
			$subscribers_page_hook_suffix = add_submenu_page(EZP_CSPE_Constants::PLUGIN_SLUG, $this->__('Coming Soon & Maintenance Elite Subscribers'), $this->__('Subscribers'), $perms, EZP_CSPE_Constants::$SUBSCRIBERS_SUBMENU_SLUG, array($this, 'display_subscribers_options_page'));

            add_action('admin_print_scripts-' . $template_page_hook_suffix, array($this, 'enqueue_scripts'));
			add_action('admin_print_scripts-' . $tools_page_hook_suffix, array($this, 'enqueue_scripts'));
            add_action('admin_print_scripts-' . $settings_page_hook_suffix, array($this, 'enqueue_scripts'));
            add_action('admin_print_scripts-' . $subscribers_page_hook_suffix, array($this, 'enqueue_scripts'));

            //Apply Styles
            add_action('admin_print_styles-' . $template_page_hook_suffix, array($this, 'enqueue_styles'));
			add_action('admin_print_styles-' . $tools_page_hook_suffix, array($this, 'enqueue_styles'));
            add_action('admin_print_styles-' . $settings_page_hook_suffix, array($this, 'enqueue_styles'));
            add_action('admin_print_styles-' . $subscribers_page_hook_suffix, array($this, 'enqueue_styles'));
        }

        // </editor-fold>

        function display_options_page($page) {

            $relative_page_path = '/../pages/' . $page;

            $__dir__ = dirname(__FILE__);

            include($__dir__ . $relative_page_path);
        }

        function display_template_options_page() {
            $this->display_options_page('page-options.php');
        }

        function display_settings_page() {
            $this->display_options_page('page-settings.php');
        }

        function display_subscribers_options_page() {
            $this->display_options_page('page-subscribers.php');
        }        

        function display_preview_page() {
            $this->display_options_page('page-preview.php');
        }

		function display_tools_page() {
			$this->display_options_page('page-tools.php');
		}
    }

}