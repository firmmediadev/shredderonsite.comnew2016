<?php


/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
require_once(dirname(__FILE__) . '/../Utilities/class-ezp-cspe-query-utility.php');

require_once('class-ezp-cspe-standard-entity-base.php');
require_once(dirname(__FILE__) . '/class-ezp-cspe-subscriber-entity.php');

require_once('class-ezp-cspe-json-entity-base.php');
require_once(dirname(__FILE__) . '/admin/class-ezp-cspe-display-entity.php');
require_once(dirname(__FILE__) . '/admin/class-ezp-cspe-content-entity.php');
require_once(dirname(__FILE__) . '/admin/class-ezp-cspe-config-entity.php');
require_once(dirname(__FILE__) . '/admin/class-ezp-cspe-set-entity.php');

if (!class_exists('EZP_CSPE_License_Status'))
{
    abstract class EZP_CSPE_License_Status
    {
        const Uncached = -2;
        const Unknown = -1;
        const Valid = 0;
        const Invalid = 1;
        const Inactive = 2;
        const Disabled = 3;
        const Site_Inactive = 4;
        const Expired = 5;
    }

}

if (!class_exists('EZP_CSPE_Global_Entity')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Global_Entity extends EZP_CSPE_JSON_Entity_Base {

        const TYPE = "EZP_CSPE_Global_Entity";

        public $active_set_index;
        public $config_index;
        public $plugin_version;
        public $license_status = EZP_CSPE_License_Status::Unknown;
        public $license_expiration_time = 0;
        
        function __construct() {

            parent::__construct();

            $plugin_version = EZP_CSPE_Constants::PLUGIN_VERSION;
        }

        public static function initialize_plugin_data() {

            $globals = EZP_CSPE_JSON_Entity_Base::get_by_type(self::TYPE);
            
            if($globals == null) {
                
                // RSR TODO: error checking here to ensure data doesnt get out of sync
                $display = EZP_CSPE_Display_Entity::create_with_defaults();

                $display->save();

                $content = EZP_CSPE_Content_Entity::create_with_defaults();

                $content->save();

                $set = EZP_CSPE_Set_Entity::create($display->id, $content->id);

                $set->save();

                $config = EZP_CSPE_Config_Entity::create_with_defaults();

                $config->save();

                $global = new EZP_CSPE_Global_Entity();

                $global->set("active_set_index", $set->id);

                $global->set("config_index", $config->id);

                $global->save();
            }
        }

        public static function get_instance() {
            
            $global = null;
            $globals = EZP_CSPE_JSON_Entity_Base::get_by_type(self::TYPE);

            if($globals != null) {
                
                $global = $globals[0];
            } 
           
            return $global;
        }
    }
}
?>