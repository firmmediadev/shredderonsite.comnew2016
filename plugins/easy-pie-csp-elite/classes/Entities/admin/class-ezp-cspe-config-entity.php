<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once(dirname(__FILE__) .  '/../class-ezp-cspe-json-entity-base.php');

if (!class_exists('EZP_CSPE_Config_Entity')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Config_Entity extends EZP_CSPE_JSON_Entity_Base {

        const TYPE = "EZP_CSPE_Config_Entity";

        public $coming_soon_mode_on;
        
        public $collect_email;
        public $collect_name;
        public $return_code;
        public $author_url;
        public $meta_description;
        public $meta_keywords;
        
        public $analytics_code;
		public $footer_scripts;
        
        public $facebook_url;
        public $google_plus_url;
        public $instagram_url;
        public $linkedin_url;
        public $pinterest_url;
        public $soundcloud_url;        
        public $twitter_url;
        public $youtube_url;
        
        
        public $unfiltered_urls;
		public $allowed_ips;
        
        public $send_email_on_subscribe = true;
		public $alerts_email_address = '';
		
        public $mailchimp_enabled = false;
        public $mailchimp_apikey = '';
        public $mailchimp_list_id = -1;
        public $mailchimp_list_name = '';
        public $mailchimp_double_optin = true;
        
        public $constant_contact_enabled = false;
//        public $constant_contact_apikey = 'czdhm9z4ddmp4vzrt4uqs69j';
//        public $constant_contact_secret = 'bVkXSkbEww6vZHtmyqV8hcrQ';
//        public $constant_contact_redirect_uri = 'http://sandbox.cms90.com/ctctredirect/index.php';
        public $constant_contact_access_token = '';
        public $constant_contact_list_id = -1;
        public $constant_contact_list_name = '';
        
        public $countdown_due_date = '';
        public $disable_after_countdown = false;
                       
        function __construct() {

			parent::__construct();
			
            $this->coming_soon_mode_on = false;
            
            $this->collect_email = true;
            $this->collect_name = true;

            $this->return_code = 503;

            $this->author_url = "";
            $this->meta_description = "";
            $this->meta_keywords = "";

            $this->analytics_code = "";
            
            $this->facebook_url = "";
            $this->twitter_url = "";
            $this->google_plus_url = "";
            
            $this->allowed_urls = "";
            // $this->constant_contact_apikey = 'czdhm9z4ddmp4vzrt4uqs69j';
            // $this->constant_contact_secret = 'bVkXSkbEww6vZHtmyqV8hcrQ';
            // $this->constant_contact_redirect_uri = 'http://sandbox.cms90.com/ctctredirect/index.php';
            $this->verifiers['alerts_email_address'] = new EZP_CSPE_Email_Verifier(true, EZP_CSPE_U::__("Alerts email address is not valid"));           
        }

        /**
         * 
         * @return EZP_CSPE_Config_Entity
         */
        public function create_with_defaults() {

            $instance = new EZP_CSPE_Config_Entity();

            return $instance;
        }

        /**
         * 
         * @param type $id
         * @return EZP_CSPE_Config_Entity
         */
        public static function get_by_id($id) {
            return EZP_CSPE_JSON_Entity_Base::get_by_id_and_type($id, self::TYPE);
        }
        
        public function fix_url_fields() {
                       
            $this->fix_url_field($this->facebook_url);                                                            
            $this->fix_url_field($this->google_plus_url);
            $this->fix_url_field($this->instagram_url);
            $this->fix_url_field($this->linkedin_url);
            $this->fix_url_field($this->pinterest_url);
            $this->fix_url_field($this->soundcloud_url);
            $this->fix_url_field($this->twitter_url);
            $this->fix_url_field($this->youtube_url);
        }
        
        private function fix_url_field(&$field) {

             if(!empty($field)) {
            
                if(strpos($field, 'http') === false) {
                    
                    $field = 'https://' . $field;
                }                    
            }
        }
    }
}
?>