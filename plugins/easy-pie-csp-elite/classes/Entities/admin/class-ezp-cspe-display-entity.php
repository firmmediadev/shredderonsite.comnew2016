<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once(dirname(__FILE__) .  '/../class-ezp-cspe-json-entity-base.php');

if (!class_exists('EZP_CSPE_Display_Background_Type'))
{
    abstract class EZP_CSPE_Display_Background_Type
    {
		const NotSet = 0;
		const Color = 1;
		const Image = 2;
		const Video = 3;
    }
}

if (!class_exists('EZP_CSPE_Display_Entity')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Display_Entity extends EZP_CSPE_JSON_Entity_Base {
    
		const GOOGLE_FONT = 'google-font';
        const TYPE = "EZP_CSPE_Display_Entity";

        public $background_image_url = "";
        public $builtin_background_image;   // Delayed initialization
        public $background_color = "#00FF00";
        public $background_tiling_enabled = false;
                              
        public $logo_width = "40%";
        public $logo_height = "";
        
		public $box_shadow_enable = true;
        public $content_box_opacity = 0.4;
        public $content_box_color = "#000000";
              
        public $text_headline_font_name = "arial";
        public $text_headline_font_size = "42px";
        public $text_headline_font_color = "#FFFFFF";
        public $text_headline_google_font = '';
		public $text_headline_google_font_effect = '';
      
        public $text_description_font_name = "arial";
        public $text_description_font_size = "20px";
        public $text_description_font_color = "#FFFFFF";
		public $text_description_google_font = '';
		public $text_description_google_font_effect = '';
      
        public $text_disclaimer_font_name = "times-new-roman";
        public $text_disclaimer_font_size = "14px";
        public $text_disclaimer_font_color = "#FFFFFF";
		public $text_disclaimer_google_font = '';
		public $text_disclaimer_google_font_effect = '';
                
        public $text_footer_font_name = "times-new-roman";
        public $text_footer_font_size = "13px";
        public $text_footer_font_color = "#FFFFFF";
		public $text_footer_google_font = '';
		public $text_footer_google_font_effect = '';
		
		public $text_thankyou_headline_font_name = "arial";
        public $text_thankyou_headline_font_size = "42px";
        public $text_thankyou_headline_font_color = "#FFFFFF";
        public $text_thankyou_headline_google_font = '';
		public $text_thankyou_headline_google_font_effect = '';
		
               
        public $email_button_width = "120px";
        public $email_button_height = "42px";
        
        public $email_button_font_name = "arial";        
        public $email_button_font_size = '16px';
        public $email_button_font_color = '#FFFFFF';
        public $email_button_google_font = '';
		public $email_button_google_font_effect = '';
        
        public $email_button_color = "#E34141";        
        
        public $youtube_video_id = '';
        public $mute_background_video = true;

        public $css = "/* This code adds a shadow around the content box */\r\n#headline { font-weight: bold }\r\n";
     
		public $background_type = EZP_CSPE_Display_Background_Type::NotSet;
		
        function __construct() {
            
            parent::__construct();
            
            $this->background_image_url = EZP_CSPE_U::$PLUGIN_URL . '/images/backgrounds/cloud-1.jpg';
            $font_size_regex = "/(px|em|ex|%|in|cm|mm|pt|pc)+$/";
            $div_size_regex = "/(px|%)+$/";
            $this->verifiers['text_headline_font_size'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_U::__("Headline font must end in a unit (px, em, etc...)"));
            $this->verifiers['text_description_font_size'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_U::__("Description font must end in a unit (px, em, etc...)"));
            $this->verifiers['text_disclaimer_font_size'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_U::__("Disclaimer font must end in a unit (px, em, etc...)"));
            $this->verifiers['text_footer_font_size'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_U::__("Footer font must end in a unit (px, em, etc...)"));
            $this->verifiers['email_button_font_size'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_U::__("Email button font must end in a unit (px, em, etc...)"));
            
//            $this->verifiers['logo_width'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_Utility::__("Logo width must end in px or %"));
//            $this->verifiers['logo_height'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_Utility::__("Logo height font must end in px or %"));
            $this->verifiers['email_button_width'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_U::__("Email button height font must end in px or %"));
            $this->verifiers['email_button_height'] = new EZP_CSPE_Regex_Verifier($font_size_regex, EZP_CSPE_U::__("Email button width must end in px or %"));
            
            $this->verifiers['content_box_opacity'] = new EZP_CSPE_Range_Verifier(0, 1, EZP_CSPE_U::__("Content box opacity must be between 0 and 1"));                                   
        }        
		
		// Prefix has to be: 
		// text_headline, text_description, text_disclaimer, text_footer, email_button, text_thankyou
		public function get_font_effect_class($prefix)
		{
			$font_name_member = $prefix . '_font_name';
			
			if($this->$font_name_member == self::GOOGLE_FONT)
			{
				$effect_member = $prefix . '_google_font_effect';
				
				$effect = $this->$effect_member;
				
				return "font-effect-$effect";
			}
			else
			{
				return '';
			}									
		}
		
		public function set_background_type()
		{
			if(trim($this->youtube_video_id) != false)
			{
				$this->background_type = EZP_CSPE_Display_Background_Type::Video;
			}
			else if(trim($this->background_image_url) != false)
			{
				$this->background_type = EZP_CSPE_Display_Background_Type::Image;
			}
			else
			{
				$this->background_type = EZP_CSPE_Display_Background_Type::Color;
			}
		}
        
        public function create_with_defaults() {
            
            $instance = new EZP_CSPE_Display_Entity();                  
        
            return $instance;
        }
        
		public static function append_google_font_family($family, $google_font)
		{
			if($family == '')
			{
				return $google_font;
			}
			else
			{
				return "$family|$google_font";
			}
		}
		
		public function get_google_font_stylesheet_text()
		{
			$family = '';
			
			if($this->text_headline_font_name == self::GOOGLE_FONT)
			{
				$family = $this->text_headline_google_font;				
			}
			
			if($this->text_description_font_name == self::GOOGLE_FONT)
			{
				$family = self::append_google_font_family($family, $this->text_description_google_font);				
			}
			
			if($this->text_disclaimer_font_name == self::GOOGLE_FONT)
			{
				$family = self::append_google_font_family($family, $this->text_disclaimer_google_font);				
			}
			
			if($this->text_footer_font_name == self::GOOGLE_FONT)
			{
				$family = self::append_google_font_family($family, $this->text_footer_google_font);				
			}
			
			if($this->email_button_font_name == self::GOOGLE_FONT)
			{
				$family = self::append_google_font_family($family, $this->email_button_google_font);				
			}
			
			if($this->text_thankyou_headline_font_name == self::GOOGLE_FONT)
			{
				$family = self::append_google_font_family($family, $this->text_thankyou_headline_google_font);				
			}
			
			if($family == '')
			{
				return '';
			}
			else
			{
				$family = str_replace(' ', '+', $family);
				$effect_string = '';
				
				$effects = array();
				
				$effects[] = $this->text_headline_google_font_effect;				
				$effects[] = $this->text_description_google_font_effect;				
				$effects[] = $this->text_disclaimer_google_font_effect;
				$effects[] = $this->text_footer_google_font_effect;
				$effects[] = $this->email_button_google_font_effect;
				$effects[] = $this->text_thankyou_headline_google_font_effect;
				
				foreach($effects as $effect)
				{
					if(($effect != 'none') && ($effect != ''))
					{
						if($effect_string != '')
						{
							$effect_string .= "|$effect";
						}
						else
						{
							$effect_string = "&effect=$effect";
						}
					}
				}
				
				return "<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=$family$effect_string'></link>";
			}
			
		}
		
        /**
         * 
         * @param type $id
         * @return EZP_CSPE_Display_Entity
         */
        public static function get_by_id($id)
        {
            return EZP_CSPE_JSON_Entity_Base::get_by_id_and_type($id, self::TYPE);            
        }    
        
        /**
         * 
         * @param type $template_key
         * @return EZP_CSPE_Display_Entity
         * Inspired from good solution found at http://stackoverflow.com/questions/5397758/json-decode-to-custom-class
         */
        public static function create_from_mixed($mixed)
        {
            // RSR TODO: Move this into a base class
            $instance = new EZP_CSPE_Display_Entity();
            
            foreach ($mixed AS $key => $value) {
                $instance->{$key} = $value;
            }
    
            return $instance;
        }
        
        /**
         * 
         * @param type $template_key
         * @return EZP_CSPE_Display_Entity
         */
        public static function create_from_template ($template_key)
        {          
            $template_directory = EZP_CSPE_U::$MINI_THEMES_TEMPLATE_DIRECTORY . $template_key;            
       
            $metadata_path = $template_directory . "/display.json";
            
            $display_json = file_get_contents($metadata_path);
            
            $mixed = json_decode($display_json);                        
            
            $new = self::create_from_mixed($mixed);           
            
            return $new;
        }          
                
        public static function display_font_field_row($label, $variable_base_name, $model, $google_font_field_id) {
                           
            $font_name_variable_name = $variable_base_name . '_font_name';
            $font_size_variable_name = $variable_base_name . '_font_size';
            $font_color_variable_name = $variable_base_name . '_font_color';                                   
                        
            $font_name_value = $model->$font_name_variable_name;
            $font_size_value = $model->$font_size_variable_name;
            $font_color_value = $model->$font_color_variable_name;
            
			$google_font_field_selector = "#$google_font_field_id td";
			
            ?>
                <th scope="row">
                    <?php EZP_CSPE_U::_e($label); ?>
                </th>
                <td>
                    <div class="compound-setting">
                        <select style="height:32px" name="<?php echo $font_name_variable_name; ?>" onChange="easyPie.CS.ChangeVisibility('<?php echo $google_font_field_selector; ?>', this.value == 'google-font');" >
                            <?php
                            
                                EZP_CSPE_Display_Entity::render_font_options($font_name_value);
                            ?>
                        </select>

                        <input name="<?php echo $font_size_variable_name;?>" class="text-styling-size" placeholder="<?php EZP_CSPE_U::_e("Size");?>" value="<?php echo $font_size_value;?>" size="6" style="line-height:24px;padding-top:1px;margin:0"/> 
                        <input name="<?php echo $font_color_variable_name;?>" class="spectrum-picker" type="text" value="<?php echo $font_color_value; ?>"/>
                    </div>
                </td>
                
            <?php                  
        }
		
		public function display_google_font_field_row($variable_base_name)
		{
			$google_font_help_text = '<a href="https://www.google.com/fonts/" target="ezpe-google-fonts">' . EZP_CSPE_U::__('Font Names') . '</a>';
			$font_name_variable_name = $variable_base_name . '_font_name';
			$google_font_variable_name = $variable_base_name . '_google_font';
			$google_font_effect_variable_name = $variable_base_name . '_google_font_effect';
			$display_render = $this->$font_name_variable_name == 'google-font' ? 'block' : 'none';
			
			?>		
			<th style="height:0;"></th>
				<td>										
					<input placeholder="<?php echo 'Google Font Name'; ?>" style='float:left; display: <?php echo $display_render; ?>' class='medium-input' name="<?php echo $google_font_variable_name; ?>" type="text" value="<?php echo $this->$google_font_variable_name; ?>" />				
					<select name="<?php echo $google_font_effect_variable_name ?>" style="display: <?php echo $display_render; ?>" >						
						<?php 
							EZP_CSPE_U::render_option('none', EZP_CSPE_U::__('No Effect'), $this->$google_font_effect_variable_name); 
							EZP_CSPE_U::render_option('3d', EZP_CSPE_U::__('3D'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('3d-float', EZP_CSPE_U::__('3D Float'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('anaglyph', EZP_CSPE_U::__('Anaglyph'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('emboss', EZP_CSPE_U::__('Emboss'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('fire', EZP_CSPE_U::__('Fire'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('fire-animation', EZP_CSPE_U::__('Fire Animation'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('neon', EZP_CSPE_U::__('Neon'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('outline', EZP_CSPE_U::__('Outline'), $this->$google_font_effect_variable_name);
							EZP_CSPE_U::render_option('shadow-multiple', EZP_CSPE_U::__('Shadow Multiple'), $this->$google_font_effect_variable_name);
						?>
					</select>
					<div style=" display: <?php echo $display_render; ?>;" class="description"><?php echo $google_font_help_text; ?></div>
				</td>
			<?php
		}
		
		
        
        public static function render_font_options($current_font_name_value)
        {
            EZP_CSPE_U::render_option("arial", EZP_CSPE_U::__("Arial"), $current_font_name_value);                        
            EZP_CSPE_U::render_option("courier-new", EZP_CSPE_U::__("Courier New"), $current_font_name_value);
            EZP_CSPE_U::render_option("comic-sans-ms", EZP_CSPE_U::__("Comic Sans MS"), $current_font_name_value);
            EZP_CSPE_U::render_option("georgia", EZP_CSPE_U::__("Georgia"), $current_font_name_value);
            EZP_CSPE_U::render_option("impact", EZP_CSPE_U::__("Impact"), $current_font_name_value);
            EZP_CSPE_U::render_option("times-new-roman", EZP_CSPE_U::__("Times New Roman"), $current_font_name_value);
            EZP_CSPE_U::render_option("verdana", EZP_CSPE_U::__("Verdana"), $current_font_name_value);                    
            EZP_CSPE_U::render_option("google-font", EZP_CSPE_U::__("Google Font"), $current_font_name_value);                    
        }
                        
        public function get_font_family_by_key($font_key, $google_font_name)
        {
            switch($font_key) {
                case "arial":
                    return "Arial, Helvetica, san-serif";
                    break;
                    
                case "courier-new":
                    return "\"Courier New\", Courier, monospace";
                    break;
                    
                case "comic-sans-ms":
                    return "\"Comic Sans MS\", cursive, sans-serif";
                    break;
                
                case "georgia":
                    return "Georgia, serif";
                    break;
                                        
                case "impact":
                    return "Impact, Charcoal, sans-serif";
                    break;
                    
                case "times-new-roman":
                    return "\"Times New Roman\", Times, serif";
                    break;
                
                case "verdana":
                    return "Verdana, Geneva, sans-serif";
                    break;    
                
				case self::GOOGLE_FONT:
					return "'{$this->$google_font_name}', sans-serif";
                
                default:
                    return "";
            }
        }
        
        
        
        public function get_font_styling($variable_base_name)
        {
            $font_name_variable_name = $variable_base_name . '_font_name';
            $font_size_variable_name = $variable_base_name . '_font_size';
            $font_color_variable_name = $variable_base_name . '_font_color';    
            $google_font_name = $variable_base_name . '_google_font';
                        
            $font_name_key_value = $this->$font_name_variable_name;
            $font_size_value = $this->$font_size_variable_name;
            $font_color_value = $this->$font_color_variable_name;            
        
			$font_family_value = EZP_CSPE_Display_Entity::get_font_family_by_key($font_name_key_value, $google_font_name);
            //$font_family_value = EZP_CSPE_Display_Entity::get_font_family_by_key($font_name_key_value);
            
            return "color: $font_color_value; font-size: $font_size_value; font-family: $font_family_value";                
        }        
    }
}
?>