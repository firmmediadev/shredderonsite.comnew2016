<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once(dirname(__FILE__) .  '/../class-ezp-cspe-json-entity-base.php');

if (!class_exists('EZP_CSPE_Content_Entity')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Content_Entity extends EZP_CSPE_JSON_Entity_Base {

        const TYPE = "EZP_CSPE_Content_Entity";

        public $logo_url = "";
        public $headline;
        public $description;
        public $disclaimer;
        public $footer;
        public $email_placeholder_text;
        public $name_placeholder_text;
        public $email_button_text;
        public $thank_you_headline;
        public $thank_you_description;
        public $title;
                
        // Deprecated - now in config
        public $countdown_due_date;
        
        public $countdown_days_text;
        public $countdown_hours_text;
        public $countdown_min_text;
        public $countdown_sec_text;

        function __construct() {

            $this->headline = EZP_CSPE_U::__("Coming soon");
            $this->description = EZP_CSPE_U::__("Our exciting new website is coming soon! Check back later.");
            $this->disclaimer = EZP_CSPE_U::__("We won't spam you or sell your email address. Pinky swear.");
            $this->footer = EZP_CSPE_U::__("(C)2016 My Company LLC");

            $this->email_placeholder_text = EZP_CSPE_U::__("Enter email");
            $this->name_placeholder_text = EZP_CSPE_U::__("Enter name");
            $this->email_button_text = EZP_CSPE_U::__("Subscribe");

            $this->thank_you_headline = EZP_CSPE_U::__("Thank you!");
            $this->thank_you_description = EZP_CSPE_U::__("You'll hear from us when we launch.");

            $this->title = EZP_CSPE_U::__("Coming soon");
            
            $this->countdown_due_date = "";
            
            $this->countdown_days_text = EZP_CSPE_U::__('Days');
            $this->countdown_hours_text = EZP_CSPE_U::__('Hrs');;
            $this->countdown_min_text = EZP_CSPE_U::__('Min');;
            $this->countdown_sec_text = EZP_CSPE_U::__('Sec');;
            
            parent::__construct();
        }

        public function create_with_defaults() {

            $instance = new EZP_CSPE_Content_Entity();

            return $instance;
        }

        /**
         * 
         * @param type $id
         * @return EZP_CSPE_Content_Entity
         */
        public static function get_by_id($id) {
            return EZP_CSPE_JSON_Entity_Base::get_by_id_and_type($id, self::TYPE);
        }
    }
}
?>
