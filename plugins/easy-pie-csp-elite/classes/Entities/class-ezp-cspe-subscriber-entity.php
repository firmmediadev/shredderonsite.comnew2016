<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once(dirname(__FILE__) .  '/class-ezp-cspe-standard-entity-base.php');

//rsr todo: rename this entity
if (!class_exists('EZP_CSPE_Contact_Entity')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Subscriber_Entity extends EZP_CSPE_Standard_Entity_Base {
 
        public $friendly_name = '';   
        public $creation_date;
        public $email_address = '';
        
        const NUMBER_PER_PAGE = 10;
        
        public static $TABLE_NAME = "easy_pie_cspe_subscribers";
        
        function __construct() {
            
            parent::__construct(self::$TABLE_NAME);
            
            $this->creation_date = date("Y-m-d H:i:s");
        }   
        
        public static function init_table() {
            
            $field_info = array();
            
            $field_info['friendly_name'] = 'varchar(255)';
            $field_info['creation_date'] = 'timestamp';
            $field_info['email_address'] = 'varchar(255)';
            
            $index_array = array();
            
            self::generic_init_table($field_info, self::$TABLE_NAME, $index_array, 'utf8', 'utf8_unicode_ci');
        } 
        
        public function get_local_creation_date()
        {
            return EZP_CSPE_U::get_simplified_local_time_from_formatted_gmt($this->creation_date, true);
        }
        
        public static function delete_by_id($id) {
        
            self::delete_by_id_and_table($id, self::$TABLE_NAME);
        }
        
        public static function get_all($page = 0)
        {
            return parent::get_all_objects(get_class(), self::$TABLE_NAME, $page);
        }
        
         public static function get_num_pages()
        {
            $contacts = self::get_all();
            
            $count = count($contacts);
                        
            $num_pages = $count / self::NUMBER_PER_PAGE;

            if(($count % self::NUMBER_PER_PAGE) != 0) {

                return floor($num_pages ) + 1;    
            } else {

                return $num_pages;
            }                                          
        }
//        
        /**
         * 
         * @param type $id
         * @return EZP_CSPE_Subscriber_Entity
         */
        public static function get_by_id($id)
        {
            return self::get_by_id_and_type($id, get_class(), self::$TABLE_NAME);
        }
		
		public static function get_by_email($email_address)
		{
			$email_address = trim($email_address);
			
			return self::get_by_unique_field_and_type('email_address', $email_address, get_class($this), self::$TABLE_NAME);
		}
    }
    
 //   EZP_CSPE_Contact_Entity::init_class();
}
?>