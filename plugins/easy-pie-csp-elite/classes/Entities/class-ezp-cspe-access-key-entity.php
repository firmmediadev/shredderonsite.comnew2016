<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once('class-ezp-cspe-json-entity-base.php');

if (!class_exists('EZP_CSPE_Access_Key_Entity')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Access_Key_Entity extends EZP_CSPE_JSON_Entity_Base {

        const TYPE = "EZP_CSPE_Access_Key_Entity";

        public $key;
        public $name = '';
       // public $expiration_date;
                       
        function __construct() {

           $this->key = hash('md5', EZP_CSPE_U::get_guid());
            
       //    $this->expiration_ticks = time() + 60 * 60 * 24 * 7;    // 1 week by default
           
            parent::__construct();
        }

        public static function get_all()
        {
           return parent::get_by_type(self::TYPE);
        }
        
        /**
         * 
         * @param type $id
         * @return EZP_CSPE_Access_Key_Entity
         */
        public static function get_by_id($id) {
            
            return EZP_CSPE_JSON_Entity_Base::get_by_id_and_type($id, self::TYPE);            
        }              
        
        public static function get_by_key($key)
        {
            //public static function get_by_type_and_field($type, $field_name, $field_value, $table_name = self::DEFAULT_TABLE_NAME)
            return parent::get_by_type_and_field(self::TYPE, 'key', $key);
        }
        
        public function get_link()
        {
            return EZP_CSPE_U::append_query_value(site_url(), EZP_CSPE_Constants::AK_QUERY_KEY, $this->key);
        }
    }
}
?>