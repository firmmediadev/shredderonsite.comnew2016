<?php


/*
  Easy Pie Coming Soon Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Easy Pie Coming Soon Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
require_once(dirname(__FILE__) . '/../class-ezp-cspe-standard-entity-base.php');
require_once(dirname(__FILE__) . '/class-ezp-legacy-cs-subscriber-entity.php');
require_once(dirname(__FILE__) . '/class-ezp-legacy-cs-contact-entity.php');
require_once(dirname(__FILE__) . '/class-ezp-legacy-cs-email-entity.php');

require_once(dirname(__FILE__) . '/../class-ezp-cspe-json-entity-base.php');
require_once(dirname(__FILE__) . '/class-ezp-legacy-cs-display-entity.php');
require_once(dirname(__FILE__) . '/class-ezp-legacy-cs-content-entity.php');
require_once(dirname(__FILE__) . '/class-ezp-legacy-cs-config-entity.php');
require_once(dirname(__FILE__) . '/class-ezp-legacy-cs-set-entity.php');

if (!class_exists('EZP_Legacy_CS_Global_Entity')) {

    /**     
     * @author Snap Creek LLC <support@snapcreek.com>
     * @copyright 2016 Snap Creek LLC
     */
    class EZP_Legacy_CS_Global_Entity extends EZP_CSPE_JSON_Entity_Base {

        const TYPE = "EZP_CS_Global_Entity";

        public $active_set_index;
        public $config_index;
        public $plugin_version;

        function __construct() {

            parent::__construct('easy_pie_cs_entities');

            $plugin_version = EZP_CS_Constants::PLUGIN_VERSION;
        }

        public static function get_instance() {
            
            $global = null;
            $globals = EZP_CSPE_JSON_Entity_Base::get_by_type(self::TYPE, 'easy_pie_cs_entities');

            if($globals != null) {
                
			//	DUP_PRO_U::log("Globals not equal to nuill");
                $global = $globals[0];
            } 
			else
			{
			//	DUP_PRO_U::log("globals are null");
			}
            return $global;
        }
    }
}
?>