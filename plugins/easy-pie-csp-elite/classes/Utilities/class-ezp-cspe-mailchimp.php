<?php
/**
 * Super-simple, minimum abstraction MailChimp API v2 wrapper
 * 
 * Uses curl if available, falls back to file_get_contents and HTTP stream.
 * This probably has more comments than code.
 *
 * Contributors:
 * Michael Minor <me@pixelbacon.com>
 * Lorna Jane Mitchell, github.com/lornajane
 * 
 * @author Drew McLellan <drew.mclellan@gmail.com> 
 * @version 1.1.1
 */
require_once(dirname(__FILE__) . '/class-ezp-cspe-utility.php');
class EZP_CSPE_MailChimp
{
    private $api_key;
    private $api_endpoint = 'https://<dc>.api.mailchimp.com/2.0';
    private $verify_ssl = false;

    /**
     * Create a new instance
     * @param string $api_key Your MailChimp API key
     */
    function __construct($api_key)
    {
        $this->api_key = $api_key;
        list(, $datacentre) = explode('-', $this->api_key);
        $this->api_endpoint = str_replace('<dc>', $datacentre, $this->api_endpoint);
    }

    /* Friendly functions
     */
    public function get_lists()
    {
        if((trim($this->api_key) == ''))
        {
            return false;            
        }
        
        $ret_val = $this->call('lists/list');

        if ($ret_val == null)
        {
            return false;
        }
        else
        {
            if(isset($ret_val['data']))
            {
                return $ret_val['data'];
            }
            else
            {
                return false;
            }            
        }
    }

    public function subscribe($list_id, $first_name, $last_name, $email, $double_optin = true)
    {
        $double_optin_string = EZP_CSPE_U::boolstring($double_optin);
        EZP_CSPE_U::log("Calling subscribe $list_id $first_name $last_name $email $this->api_key doubleoptin:$double_optin_string");

        $result = $this->call('lists/subscribe', array(
            'id' => $list_id,
            'email' => array('email' => $email),
            'merge_vars' => array('FNAME' => $first_name, 'LNAME' => $last_name),
            'double_optin' => $double_optin_string,
            'update_existing' => 'true',
            'replace_interests' => 'false',
            'send_welcome' => 'false',
        ));        

        EZP_CSPE_U::log_object('called mailchimp subscribe got', $result);

        $ret_val = true;

        if (isset($result->status) && ($result->status == 'error'))
        {
            EZP_CSPE_U::log("Got error when calling mailchimp lists/subscribe with list id $list_id");

            $ret_val = false;
        }

        return $ret_val;
    }

    /**
     * Call an API method. Every request needs the API key, so that is added automatically -- you don't need to pass it in.
     * @param  string $method The API method to call, e.g. 'lists/list'
     * @param  array  $args   An array of arguments to pass to the method. Will be json-encoded for you.
     * @return array          Associative array of json decoded API response.
     */
    public function call($method, $args = array(), $timeout = 10)
    {
        return $this->makeRequest($method, $args, $timeout);
    }

    /**
     * Performs the underlying HTTP request. Not very exciting
     * @param  string $method The API method to be called
     * @param  array  $args   Assoc array of parameters to be passed
     * @return array          Assoc array of decoded result
     */
    private function makeRequest($method, $args = array(), $timeout = 10)
    {
        $args['apikey'] = $this->api_key;

        $url = $this->api_endpoint . '/' . $method . '.json';
        $json_data = json_encode($args);

        if (function_exists('curl_init') && function_exists('curl_setopt'))
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            $result = curl_exec($ch);
            curl_close($ch);
        }
        else
        {
            $result = file_get_contents($url, null, stream_context_create(array(
                'http' => array(
                    'protocol_version' => 1.1,
                    'user_agent' => 'PHP-MCAPI/2.0',
                    'method' => 'POST',
                    'header' => "Content-type: application/json\r\n" .
                    "Connection: close\r\n" .
                    "Content-length: " . strlen($json_data) . "\r\n",
                    'content' => $json_data,
                ),
            )));
		}
		
        return $result ? json_decode($result, true) : false;
    }

}
