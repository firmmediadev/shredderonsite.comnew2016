<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once(dirname(__FILE__) .  '/../Utilities/class-ezp-cspe-utility.php');
require_once(dirname(__FILE__) .  '/../Entities/class-ezp-cspe-subscriber-entity.php');
require_once(dirname(__FILE__) .  '/../Utilities/class-ezp-cspe-mailchimp.php');

if(EZP_CSPE_U::PHP53())
{
	require_once(dirname(__FILE__) .  '/../Utilities/class-ezp-cspe-constant-contact.php');
	require_once(dirname(__FILE__) .  '/../../lib/Ctct/ConstantContact-Config.php');
}

if (!class_exists('EZP_CSPE_Mail_Provider_Type'))
{
	// Make this a bit field so we can use or flags
    abstract class EZP_CSPE_Mail_Provider_Type
    {
        const MailChimp = 1;		
        const ConstantContact = 2;        
		const All = 3;
    }
}

if (!class_exists('EZP_CSPE_Email_Utility')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Email_Utility {
 
        // [0] is first name, [1] is last
        public static function split_names($friendly_name)
        {
            $names = explode(' ', $friendly_name);

            $first_name = '';
            $last_name = '';

            $num_names = count($names);


            if($num_names == 1)
            {
                $first_name = $names[0];
            }
            else if(count($names) > 1)
            {
                $first_name = $names[0];

                $subarray = array_slice($names, 1);

                $last_name = implode(' ', $subarray);
            }
            else
            {
                // Nothing to do
                EZP_CSPE_U::log("Invalid name given when attempting to subscribe: " . $friendly_name);

                return null;
            }

            return array(0 => $first_name, 1 => $last_name);
        }
        
        public static function send_admin_subscribe_email($subscriber)
        {
			/* @var $global EZP_CSPE_Global_Entity */
			$global = EZP_CSPE_Global_Entity::get_instance();
            
            if($global != null)
            {
                /* @var $config EZP_CSPE_Config_Entity */
                $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);

				/* @var $subscriber EZP_CSPE_Subscriber_Entity */
				$to = $config->alerts_email_address;

				if(trim($to) == '')
				{
					$to = get_option('admin_email');
				}

				if (empty($to) === false)
				{
					EZP_CSPE_U::log("Attempting to send subscriber alert to $to");

					$subject = sprintf(EZP_CSPE_U::__('New subscriber alert for %1$s'), home_url());
					$message = EZP_CSPE_U::__('Someone just signed up on your coming soon & maintenance page!');

					$message .= "<br/><br/>";

					$message .= '<strong>' . EZP_CSPE_U::__('Name') . ': </strong>' . $subscriber->friendly_name;
					$message .= '<br/>';
					$message .= '<strong>' . EZP_CSPE_U::__('Email') . ': </strong>' . $subscriber->email_address;

					if (wp_mail($to, $subject, $message, array('Content-Type: text/html; charset=UTF-8')))
					{
						// ok                    
					}
					else
					{
						EZP_CSPE_U::log("Error sending new subscriber alert email to $to regarding $subscriber->friendly_name");
					}
				}
				else
				{
					EZP_CSPE_U::log("No admin email so not sending subscribe alert for $subscriber->friendly_name");
				}
			}
        }
				
		public static function push_subscriber_list_to_mail_providers($mail_providers = EZP_CSPE_Mail_Provider_Type::All)
		{
			$subscribers = EZP_CSPE_Subscriber_Entity::get_all();
			
			$error_list = self::add_subscribers_to_mail_providers($subscribers, $mail_providers);
			
			return $error_list;			
		}
        
		//-- Methods that bridge the subscriber entity with the providers
		// Returns array of errors
		public static function add_subscriber_to_mail_providers($subscriber, $mail_providers = EZP_CSPE_Mail_Provider_Type::All)
		{
			$subscribers = array();
			
			array_push($subscribers, $subscriber);
			
			return self::add_subscribers_to_mail_providers($subscribers, $mail_providers);
		}
		
		// Returns array of errors
        public static function add_subscribers_to_mail_providers($subscribers, $mail_providers = EZP_CSPE_Mail_Provider_Type::All)
        {
			$error_list = array();
			
			/* @var $global EZP_CSPE_Global_Entity */
            $global = EZP_CSPE_Global_Entity::get_instance();
            
            if($global != null)
            {
                /* @var $config EZP_CSPE_Config_Entity */
                $config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
                                
				foreach($subscribers as $subscriber)
				{
					/* @var $subscriber EZP_CSPE_Subscriber_Entity */
					$names = self::split_names($subscriber->friendly_name);

					if($names != null)
					{						
						$first_name = $names[0];
						$last_name = $names[1];

						if(($mail_providers & EZP_CSPE_Mail_Provider_Type::MailChimp != 0) && ($config->mailchimp_enabled) && ($config->mailchimp_list_id != -1))
						{
							if(!self::add_subscriber_to_mailchimp($config, $first_name, $last_name, $subscriber->email_address))
							{
								$error_message = printf(EZP_CSPE_U::__("Error adding %s (%s) to MailChimp.", $subscriber->email_address, $subscriber->friendly_name));
								
								EZP_CSPE_U::log($error_message);
								array_push($error_list, $error_message);
							}
						}

                        if(($mail_providers & EZP_CSPE_Mail_Provider_Type::ConstantContact != 0) && ($config->constant_contact_enabled) && ($config->constant_contact_list_id != -1))
                        {
                            if(!self::add_subscriber_to_constant_contact($config, $first_name, $last_name, $subscriber->email_address))
                            {
                                $error_message = printf(EZP_CSPE_U::__("Error adding %s (%s) to ConstantContact.", $subscriber->email_address, $subscriber->friendly_name));
                                
                                EZP_CSPE_U::log($error_message);
                                array_push($error_list, $error_message);
                            }
                        }
					}
					else
					{
						EZP_CSPE_U::log("Couldn't split friendly name of subscriber with id $subscriber->id");
					}										
                }                
            }
			
			return $error_list;
        }
						
		public static function add_subscriber_to_mailchimp($config, $first_name, $last_name, $email_address)
        {
			/* @var $config EZP_CSPE_Config_Entity */
			$mailchimp = new EZP_CSPE_MailChimp($config->mailchimp_apikey);
				
			return $mailchimp->subscribe($config->mailchimp_list_id, $first_name, $last_name, $email_address, $config->mailchimp_double_optin);
        }

        public static function add_subscriber_to_constant_contact($config, $first_name, $last_name, $email_address)
        {
            /* @var $config EZP_CSPE_Config_Entity */           
			 $constant_contact = new EZP_CSPE_Constant_Contact(CTCT_APIKEY, $config->constant_contact_access_token);
            			  
            return $constant_contact->subscribe($config->constant_contact_list_id, $first_name, $last_name, $email_address);
        }
    }
}
?>
