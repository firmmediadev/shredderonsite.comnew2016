<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once('class-ezp-cspe-query-utility.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-global-entity.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-config-entity.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-contact-entity.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-content-entity.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-display-entity.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-email-entity.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-set-entity.php');
require_once(dirname(__FILE__) .  '/../Entities/legacy/class-ezp-legacy-cs-subscriber-entity.php');

if (!class_exists('EZP_CSPE_Import_Utility')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Import_Utility {
              
		public static function is_ezp_cs_installed()
		{
			return is_plugin_active('easy-pie-coming-soon') || is_plugin_inactive('easy-pie-coming-soon');
		}
		
		public static function import_ezp_cs_settings()
		{
			/*----------- Legacy Entities ---------------*/
			/* @var $legacy_global EZP_CSPE_Global_Entity */
			$legacy_global = EZP_Legacy_CS_Global_Entity::get_instance();
			
			/* @var $legacy_config EZP_Legacy_CS_Config_Entity */
			$legacy_config = EZP_Legacy_CS_Config_Entity::get_by_id($legacy_global->config_index);
			
			/* @var $legacy_set EZP_Legacy_CS_Set_Entity */
			$legacy_set = EZP_Legacy_CS_Set_Entity::get_by_id($legacy_global->active_set_index);
			
			/* @var $legacy_display EZP_Legacy_CS_Display_Entity */
			$legacy_display = EZP_Legacy_CS_Display_Entity::get_by_id($legacy_set->display_index);
			
			/* @var $legacy_content EZP_Legacy_CS_Content_Entity */
			$legacy_content = EZP_Legacy_CS_Content_Entity::get_by_id($legacy_set->content_index);
			
			/*DUP_PRO_U::log_object('global', $legacy_global);
			DUP_PRO_U::log_object('config', $legacy_config);
			DUP_PRO_U::log_object('set', $legacy_set);
			DUP_PRO_U::log_object('display', $legacy_display);
			DUP_PRO_U::log_object('content', $legacy_content);*/
			
			/*--------- Elite Entities ------------*/
			/* @var $global EZP_CSPE_Global_Entity */
			$global = EZP_CSPE_Global_Entity::get_instance();
			
			/* @var $config EZP_CSPE_Config_Entity */
			$config = EZP_CSPE_Config_Entity::get_by_id($global->config_index);
						
			/* @var $set EZP_CSPE_Set_Entity */
			$set = EZP_CSPE_Set_Entity::get_by_id($global->active_set_index);
			
			/* @var $display EZP_CSPE_Display_Entity */
			$display = EZP_CSPE_Display_Entity::get_by_id($set->display_index);
			
			/* @var $content EZP_CSPE_Content_Entity */
			$content = EZP_CSPE_Content_Entity::get_by_id($set->content_index);
			
			
			$config->collect_email = $legacy_config->collect_email;
			$config->collect_name = $legacy_config->collect_name;
			$config->return_code = $legacy_config->return_code;
			$config->author_url = $legacy_config->author_url;
			$config->meta_description = $legacy_config->meta_description;
			$config->meta_keywords = $legacy_config->meta_keywords;
			$config->analytics_code = $legacy_config->analytics_code;
			$config->facebook_url = $legacy_config->facebook_url;
			$config->twitter_url = $legacy_config->twitter_url;
			$config->google_plus_url = $legacy_config->google_plus_url;
			$config->unfiltered_urls = $legacy_config->unfiltered_urls;
			$config->countdown_due_date = $legacy_content->countdown_due_date;	// Switched where this was located
			
			$config->save();
			
			$content->logo_url = $legacy_content->logo_url;
			$content->headline = $legacy_content->headline;
			$content->description = $legacy_content->description;
			$content->disclaimer = $legacy_content->disclaimer;
			$content->footer = $legacy_content->footer;
			$content->email_placeholder_text = $legacy_content->email_placeholder_text;
			$content->name_placeholder_text = $legacy_content->name_placeholder_text;
			$content->email_button_text = $legacy_content->email_button_text;
			$content->thank_you_headline = $legacy_content->thank_you_headline;
			$content->thank_you_description = $legacy_content->thank_you_description;
			$content->title = $legacy_content->title;			
			
			$content->save();
								
			$display->background_image_url = $legacy_display->background_image_url;
			$display->builtin_background_image = $legacy_display->builtin_background_image;
			$display->background_color = $legacy_display->background_color;
			$display->background_tiling_enabled = $legacy_display->background_tiling_enabled;
			$display->logo_width = $legacy_display->logo_width;
			$display->logo_height = $legacy_display->logo_height;
			$display->content_box_opacity = $legacy_display->content_box_opacity;
			$display->content_box_color = $legacy_display->content_box_color;
			$display->text_headline_font_name = $legacy_display->text_headline_font_name;
			$display->text_headline_font_size = $legacy_display->text_headline_font_size;
			$display->text_headline_font_color = $legacy_display->text_headline_font_color;
			$display->text_description_font_name = $legacy_display->text_description_font_name;
			$display->text_description_font_size = $legacy_display->text_description_font_size;
			$display->text_description_font_color = $legacy_display->text_description_font_color;
			$display->text_disclaimer_font_name = $legacy_display->text_disclaimer_font_name;
			$display->text_disclaimer_font_size = $legacy_display->text_disclaimer_font_size;
			$display->text_disclaimer_font_color = $legacy_display->text_disclaimer_font_color;
			$display->text_footer_font_name = $legacy_display->text_footer_font_color;
			$display->text_footer_font_size = $legacy_display->text_footer_font_size;
			$display->text_footer_font_color = $legacy_display->text_footer_font_color;
			$display->email_button_width = $legacy_display->email_button_width;
			$display->email_button_height = $legacy_display->email_button_height;
			$display->email_button_font_name = $legacy_display->email_button_font_name;
			$display->email_button_font_size = $legacy_display->email_button_font_size;
			$display->email_button_font_color = $legacy_display->email_button_font_color;
			$display->email_button_color = $legacy_display->email_button_color;
			$display->css = $legacy_display->css;
			$display->youtube_video_id = '';
					
			$display->set_background_type();
			
			$display->save();					
		}
		
		public static function import_ezp_cs_subscribers()
		{
			$subscribers_added = 0;
			$legacy_contacts = EZP_Legacy_CS_Contact_Entity::get_all();
			
			foreach($legacy_contacts as $legacy_contact)
			{
				/* @var $legacy_contact EZP_Legacy_CS_Contact_Entity */
				/* @var $legacy_email EZP_Legacy_CS_Email_Entity */
				$legacy_email = EZP_Legacy_CS_Email_Entity::get_by_contact_id($legacy_contact->id);
				
				$existing_subscriber = EZP_CSPE_Subscriber_Entity::get_by_email($legacy_email->email_address);
						
				if($existing_subscriber == null)
				{
					$subscriber = new EZP_CSPE_Subscriber_Entity();

					$subscriber->friendly_name = $legacy_contact->friendly_name;
					$subscriber->creation_date = $legacy_contact->creation_date;												
					$subscriber->email_address = $legacy_email->email_address;

					$subscriber->save();
					$subscribers_added++;
				}
				else
				{
					EZP_CSPE_U::log_object("subscriber already exists for {$legacy_email->email_address}", $existing_subscriber);
				}
			}
			
			return $subscribers_added;
		}
    }
}
?>