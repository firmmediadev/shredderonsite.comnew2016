<?php
/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once(dirname(__FILE__) . '/../class-ezp-cspe-constants.php');

if (!class_exists('EZP_CSPE_License_Status'))
{
    abstract class EZP_CSPE_License_Status
    {
        const Unknown = -1;
        const Valid = 0;
        const Invalid = 1;
        const Inactive = 2;
        const Disabled = 3;
        const Site_Inactive = 4;
        const Expired = 5;

    }

}

if (!class_exists('EZP_CSPE_License_Utility'))
{

    /**
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2014 Snap Creek LLC
     */
    class EZP_CSPE_License_Utility
    {
        // Pseudo constants
        public static $LicenseCacheTime;
        
        public static function init()
        {
            $hours = 72;
            self::$LicenseCacheTime = $hours * 3600;
         //   self::$LicenseCacheTime = 60;
        }
        
        public static function change_license_activation($activate)
        {
            $license = get_option(EZP_CSPE_Constants::LICENSE_KEY_OPTION_NAME, '');

            if ($activate)
            {
                $api_params = array(
                    'edd_action' => 'activate_license',
                    'license' => $license,
                    'item_name' => urlencode(EZP_CSPE_ITEM_NAME), // the name of our product in EDD,
                    'url' => home_url()
                );
            }
            else
            {
                $api_params = array(
                    'edd_action' => 'deactivate_license',
                    'license' => $license,
                    'item_name' => urlencode(EZP_CSPE_ITEM_NAME), // the name of our product in EDD,
					'url' => home_url()
				);
            }

			global $wp_version;

			$agent_string = "WordPress/" . $wp_version;
			
            // Call the custom API.
            $response = wp_remote_post( EZP_CSPE_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'user-agent' => $agent_string, 'body' => $api_params ) );
            
            // make sure the response came back okay
            if (is_wp_error($response))
            {
                if ($activate)
                {
                    $action = 'activating';
                }
                else
                {
                    $action = 'deactivating';
                }

                EZP_CSPE_U::log_object("Error $action $license", $response);

                return false;
            }

            $license_data = json_decode(wp_remote_retrieve_body($response));
                        
            if ($activate)
            {
                // decode the license data                
                if ($license_data->license == 'valid')
                {
                    EZP_CSPE_U::log("Activated license $license");
                    return true;
                }
                else
                {
                    EZP_CSPE_U::log_object("Problem activating license $license", $license_data);
                    return false;
                }
            }
            else
            {                
                if ($license_data->license == 'deactivated')
                {
                    EZP_CSPE_U::log("Deactivated license $license");
                    return true;
                }
                else
                {
                    // problems activating
                    //update_option('edd_sample_license_status', $license_data->license);
                    EZP_CSPE_U::log_object("Problems deactivating license $license", $license_data);
                    return false;
                }
            }
        }

        public static function get_license_status($force_refresh)
        {
       //rsr      EZP_CSPE_U::log("get license status");
            /* @var $global EZP_CSPE_Global_Entity */
            $global = EZP_CSPE_Global_Entity::get_instance();
            
            if ($force_refresh === false)
            {          
                if(time() > $global->license_expiration_time)
                {
                    EZP_CSPE_U::log("Uncaching license because current time = " . time() . " and expiration time = {$global->license_expiration_time}");
                    $global->license_status = EZP_CSPE_License_Status::Uncached;
                }
            }
            else
            {
                EZP_CSPE_U::log("forcing live license update");
                $global->license_status = EZP_CSPE_License_Status::Uncached;
            }
            
            if ($global->license_status == EZP_CSPE_License_Status::Uncached)
            {
                EZP_CSPE_U::log("retrieving live license status");
                
                $license_key = get_option(EZP_CSPE_Constants::LICENSE_KEY_OPTION_NAME, '');
                
                if ($license_key != '')
                {
                    $api_params = array(
                        'edd_action' => 'check_license',
                        'license' => $license_key,
                        'item_name' => urlencode(EZP_CSPE_ITEM_NAME),
						'url' => home_url()
                    );

					global $wp_version;
					$agent_string = "WordPress/" . $wp_version;

                    $response = wp_remote_post( EZP_CSPE_STORE_URL, array( 'timeout' => 15, 'sslverify' => false,  'user-agent' => $agent_string, 'body' => $api_params ) );
        
                    if (is_wp_error($response))
                    {
                        $global->license_status = EZP_CSPE_License_Status::Unknown;
                        EZP_CSPE_U::log_object("Error getting license check response for $license_key", $response);                                                
                    }
                    else
                    {
                        $license_data = json_decode(wp_remote_retrieve_body($response));

                        $global->license_status = self::get_license_status_from_string($license_data->license);

                        if ($global->license_status == EZP_CSPE_License_Status::Unknown)
                        {
                            EZP_CSPE_U::log("Problem retrieving license status for $license_key");
                        }
                    }
                }
                else
                {
                    $global->license_status = EZP_CSPE_License_Status::Invalid;
                }
   
                $global->license_expiration_time = time() + self::$LicenseCacheTime;
                
                $global->save();
            }

            return $global->license_status;
        }

        private static function get_license_status_from_string($license_status_string)
        {
            switch ($license_status_string)
            {
                case 'valid':
                    return EZP_CSPE_License_Status::Valid;
                    break;

                case 'invalid':
                    return EZP_CSPE_License_Status::Invalid;

                case 'expired':
                    return EZP_CSPE_License_Status::Expired;

                case 'disabled':
                    return EZP_CSPE_License_Status::Disabled;

                case 'site_inactive':
                    return EZP_CSPE_License_Status::Site_Inactive;

                case 'expired':
                    return EZP_CSPE_License_Status::Expired;

                default:
                    return EZP_CSPE_License_Status::Unknown;
            }
        }
        
        public static function get_license_status_string($license_status_string)
        {
            switch ($license_status_string)
            {
                case EZP_CSPE_License_Status::Valid:
                    return EZP_CSPE_U::__('Valid');

                case EZP_CSPE_License_Status::Invalid:
                    return EZP_CSPE_U::__('Invalid');

                case EZP_CSPE_License_Status::Expired:
                    return EZP_CSPE_U::__('Expired');

                case EZP_CSPE_License_Status::Disabled:
                    return EZP_CSPE_U::__('Disabled');

                case EZP_CSPE_License_Status::Site_Inactive:
                    return EZP_CSPE_U::__('Site Inactive');

                case EZP_CSPE_License_Status::Expired:
                    return EZP_CSPE_U::__('Expired');

                default:
                    return EZP_CSPE_U::__('Unknown');
            }
        }
    }

    EZP_CSPE_License_Utility::init();
}
?>
