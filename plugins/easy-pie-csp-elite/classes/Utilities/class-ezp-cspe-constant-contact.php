<?php
require_once(dirname(__FILE__) . '/class-ezp-cspe-utility.php');


if (!class_exists('Ctct\\ConstantContact'))
{
	require_once dirname(dirname(dirname(__FILE__))) . '/lib/Ctct/autoload.php';
}

use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;

// use Ctct\Components\Contacts\CustomField;
// use Ctct\Components\Contacts\ContactList;
// use Ctct\Components\Contacts\EmailAddress;
// use Ctct\Exceptions\CtctException;

class EZP_CSPE_Constant_Contact
{
	/* : 
	  get api_key and set redirect_uri on constantcontact.mashery.com

	  api_key
	  https://constantcontact.mashery.com/apps/mykeys

	  redirect_uri
	  https://constantcontact.mashery.com/apps/myapps

	  and put the redirect file on your own server
	  file content:
	  <?php
	  #https://community.constantcontact.com/t5/Developer-Support-ask-questions/Get-Access-Token-manually-vs-OAuth-2-0/td-p/266820
	  if ( isset( $_REQUEST['redirect'] ) && isset( $_SERVER['QUERY_STRING'] ) ) {
	  header( "Location:" . $_REQUEST['redirect'] . "?" . $_SERVER['QUERY_STRING'] );
	  exit;
	  }
	 */
	// private $api_key='czdhm9z4ddmp4vzrt4uqs69j';
	// private $redirect_uri='http://sandbox.cms90.com/ctctredirect/index.php';
	private $api_key;
	private $access_token;
	// private $api_endpoint = 'https://api.constantcontact.com/v2';
	private $api;

	/**
	 * Create a new instance
	 * @param string $api_key Your MailChimp API key
	 */
	function __construct($api_key, $access_token)
	{
		$this->api_key = $api_key;
		$this->access_token = $access_token;
		$this->api = new ConstantContact($api_key);
	}

	public static function create_oath2($clientId, $clientSecret, $redirectUri)
	{
		return new Ctct\Auth\CtctOAuth2($clientId, $clientSecret, $redirectUri);			
	}
	
	/* Friendly functions
	 */
	public function get_lists()
	{
		if ((trim($this->api_key) == '') || (trim($this->access_token) == ''))
		{
			return false;
		}

		$results = $this->api->getLists($this->access_token);
		$lists = array();
		foreach ($results as $key => $list_obj)
		{
			$lists[] = get_object_vars($list_obj);
		}
		return $lists;
	}

	public function subscribe($list_id, $first_name, $last_name, $email, $double_optin = true)
	{
		// $double_optin_string = EZP_CSPE_U::boolstring($double_optin);
		$access_token = $this->access_token;
		EZP_CSPE_U::log("Calling subscribe $list_id $first_name $last_name $email {$this->access_token} access_token:$access_token");
		$api = $this->api;
		$ret_val = false;

		try
		{
			// check to see if a contact with the email addess already exists in the account
			$response = $api->getContactByEmail($access_token, $email);
			EZP_CSPE_U::log_object('Getting Contact By Email Address', $response);
			$is_new_contact = empty($response->results);
			// create a new contact if one does not exist
			if ($is_new_contact)
			{
				$contact = new Contact();
				$contact->addEmail($email);
				$contact->first_name = $first_name;
				$contact->last_name = $last_name;
			}
			else
			{
				$contact = $response->results[0];
			}

			$contact->addList($list_id);
			if ($is_new_contact)
			{
				$response = $api->addContact($access_token, $contact);
				EZP_CSPE_U::log_object('Adding Contact', $response);
			}
			else
			{
				$response = $api->updateContact($access_token, $contact);
				EZP_CSPE_U::log_object('Updating Contact', $response);
			}
			$ret_val = true;
		}

		//Catch any errors so the user can't see them.
		catch (CtctException $ex)
		{
			EZP_CSPE_U::log_object('error adding user to Constant Contact', $ex);
		}
		return $ret_val;
	}

}
