<?php

/*
  Coming Soon & Maintenance Elite Plugin
  Copyright (C) 2016, Snap Creek LLC
  website: snapcreek.com contact: support@snapcreek.com

  Coming Soon & Maintenance Elite Plugin is distributed under the GNU General Public License, Version 3,
  June 2007. Copyright (C) 2007 Free Software Foundation, Inc., 51 Franklin
  St, Fifth Floor, Boston, MA 02110, USA
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

if (!class_exists('EZP_CSPE_Constants')) {

    /**     
     * @author Bob Riley <support@snapcreek.com>
     * @copyright 2015 Snap Creek LLC
     */
    class EZP_CSPE_Constants {

        /* Access control */
        const AK_QUERY_KEY = 'ezp_cspe_ak';  
        const AK_COOKIE_NAME = 'ezp_cspe_ak';
        
        const PLUGIN_SLUG = 'easy-pie-csp-elite';
        const PLUGIN_VERSION = "2.1.2"; // RSR Version
        

        const LICENSE_STATUS_TRANSIENT_NAME = 'easy_pie_cspe_ls';       
        const LICENSE_KEY_OPTION_NAME = 'easy_pie_cspe_license_key';       
        
        const PLUGIN_VERSION_OPTION_KEY = "easy_pie_cspe_version"; // RSR Version

        /* Pseudo constants */
        public static $PLUGIN_DIR;
        public static $TEMPLATE_SUBMENU_SLUG;
		public static $TOOLS_SUBMENU_SLUG;
        public static $SETTINGS_SUBMENU_SLUG;
        public static $SUBSCRIBERS_SUBMENU_SLUG;
        public static $PREVIEW_SUBMENU_SLUG;
        public static $COMING_SOON_PRO_SUBMENU_SLUG;

        public static function init() {

            $__dir__ = dirname(__FILE__);
            
            self::$PLUGIN_DIR = $__dir__ . "../" . self::PLUGIN_SLUG;
            
            self::$TEMPLATE_SUBMENU_SLUG = EZP_CSPE_Constants::PLUGIN_SLUG;
			self::$TOOLS_SUBMENU_SLUG = EZP_CSPE_Constants::PLUGIN_SLUG . '-tools';
            self::$SETTINGS_SUBMENU_SLUG = EZP_CSPE_Constants::PLUGIN_SLUG . '-settings';
            self::$SUBSCRIBERS_SUBMENU_SLUG = EZP_CSPE_Constants::PLUGIN_SLUG . '-subscribers';            
            self::$PREVIEW_SUBMENU_SLUG = EZP_CSPE_Constants::PLUGIN_SLUG . '-view';            
        }

    }

    EZP_CSPE_Constants::init();
}
?>
